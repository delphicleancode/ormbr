{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dml.generator.mssql;

interface

uses
  SysUtils,
  Rtti,
  ormbr.dml.generator,
  ormbr.mapping.classes,
  ormbr.factory.interfaces,
  ormbr.driver.register,
  ormbr.types.database,
  ormbr.dml.commands,
  ormbr.criteria;

type
  /// <summary>
  /// Classe de conex�o concreta com dbExpress
  /// </summary>
  TDMLGeneratorMSSql = class(TDMLGeneratorAbstract)
  protected
    function GetGeneratorSelect(ACriteria: ICriteria): string; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    function GeneratorSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; override;
    function GeneratorSequenceCurrentValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64; override;
    function GeneratorSequenceNextValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64; override;
  end;

implementation

{ TDMLGeneratorMSSql }

constructor TDMLGeneratorMSSql.Create;
begin
  inherited;
  FDateFormat := 'dd/MM/yyyy';
  FTimeFormat := 'HH:MM:SS';
end;

destructor TDMLGeneratorMSSql.Destroy;
begin

  inherited;
end;

function TDMLGeneratorMSSql.GetGeneratorSelect(ACriteria: ICriteria): string;
var
  sTable: string;
begin
   inherited;
   sTable := ACriteria.AST.Select.TableNames.Columns[0].Name;
   ACriteria.SelectSection(secSelect);
   ACriteria.Column('ROW_NUMBER() OVER(ORDER BY CURRENT_TIMESTAMP) AS ROWNUMBER');
   ACriteria.AST.Select.TableNames.Clear;
   ACriteria.From(sTable + ')').&As(sTable);
   ACriteria.SelectSection(secWhere);
   ACriteria.Where('(ROWNUMBER > %u) AND (ROWNUMBER <= %u)');
   ACriteria.SelectSection(secOrderBy);
   Result := 'SELECT * FROM (' + ACriteria.AsString;
end;

function TDMLGeneratorMSSql.GeneratorSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string;
var
  oCriteria: ICriteria;
begin
   oCriteria := GetCriteriaSelect(AClass, AID);
   if APageSize > -1 then
      Result := GetGeneratorSelect(oCriteria)
   else
      Result := oCriteria.AsString;
end;

function TDMLGeneratorMSSql.GeneratorSequenceCurrentValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64;
begin
  Result := ExecuteSequence(Format('SELECT CURRENT_VALUE FROM SYS.SEQUENCES WHERE NAME = ''%s''', [ACommandInsert.Sequence.Name]));
end;

function TDMLGeneratorMSSql.GeneratorSequenceNextValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64;
begin
  Result := ExecuteSequence(Format('SELECT NEXT VALUE FOR %s ', [ACommandInsert.Sequence.Name]));
end;

initialization
  TDriverRegister.RegisterDriver(dnMSSQL, TDMLGeneratorMSSql.Create);

end.
