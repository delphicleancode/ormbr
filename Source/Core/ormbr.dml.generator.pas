{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dml.generator;

interface

uses
  DB,
  Rtti,
  SysUtils,
  StrUtils,
  Variants,
  TypInfo,
  Generics.Collections,
  ormbr.mapping.classes,
  ormbr.mapping.explorer,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  ormbr.factory.interfaces,
  ormbr.dml.interfaces,
  ormbr.criteria,
  ormbr.dml.commands;

type
  /// <summary>
  /// Classe de conex�es abstract
  /// </summary>
  TDMLGeneratorAbstract = class abstract(TInterfacedObject, IDMLGeneratorCommand)
  private
    function GetPropertyValue(AObject: TObject; AProperty: TRttiProperty): Variant;
    function IsNull(AProperty: TRttiProperty; AObject: TObject): Boolean;
    procedure SetJoinColumn(AClass: TClass; ATable: TTableMapping; ACriteria: ICriteria);
  protected
    FConnection: IDBConnection;
    FDateFormat: string;
    FTimeFormat: string;
    function GetCriteriaSelect(AClass: TClass; AID: TValue): ICriteria; virtual;
    function GetGeneratorSelect(ACriteria: ICriteria): string; virtual;
    function ExecuteSequence(ASQL: string): Int64;
  public
    constructor Create; virtual; abstract;
    procedure SetConnection(const AConnaction: IDBConnection); virtual;
    function GeneratorSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string; virtual; abstract;
    function GeneratorSelect(ASQL: ICriteria; APageSize: Integer): string; virtual;
    function GeneratorUpdate(AObject: TObject): string; overload; virtual;
    function GeneratorUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string; overload; virtual;
    function GeneratorInsert(AObject: TObject; ACommandInsert: TDMLCommandInsert): string; virtual;
    function GeneratorDelete(AObject: TObject): string; virtual;
    function GeneratorSequenceCurrentValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64; virtual; abstract;
    function GeneratorSequenceNextValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64; virtual; abstract;
  end;

implementation

uses
  ormbr.mapping.rttiutils;

{ TDMLGeneratorAbstract }

function TDMLGeneratorAbstract.ExecuteSequence(ASQL: string): Int64;
var
  oDBResultSet: IDBResultSet;
begin
  oDBResultSet := FConnection.ExecuteSQL(ASQL);
  if oDBResultSet.RecordCount > 0 then
    Result := VarAsType(oDBResultSet.GetFieldValue(0), varInt64)
  else
    Result := 0;
end;

function TDMLGeneratorAbstract.GeneratorDelete(AObject: TObject): string;
var
  oTable: Table;
  oProperty: TRttiProperty;
  oCriteria: ICriteria;
begin
  oCriteria := CreateCriteria.Delete;
  oTable := AObject.GetTable;
  oCriteria.From(oTable.Name);
  for oProperty in AObject.GetPrimaryKey do
    oCriteria.Where(oProperty.Name + '=' + GetPropertyValue(AObject, oProperty));
  Result := oCriteria.AsString;
end;

function TDMLGeneratorAbstract.GeneratorInsert(AObject: TObject; ACommandInsert: TDMLCommandInsert): string;
var
  oTable: Table;
  oRestAttr: TCustomAttribute;
  oPrimaryKey: PrimaryKey;
  oProperty: TRttiProperty;
  oCriteria: ICriteria;
begin
  Result := '';
  oCriteria := CreateCriteria;
  oTable := AObject.GetTable;
  oCriteria.Insert.Into(oTable.Name);
  for oProperty in AObject.GetColumns do
  begin
    if IsNull(oProperty, AObject) then
      Continue;
    /// Restrictions [Required, Unique, Check, NoInsert, NoUpdate, NotNull, Hidden]
    oRestAttr := AObject.GetRestriction(oProperty);
    if oRestAttr <> nil then
      if NoInsert in Restrictions(oRestAttr).Restrictions then
        Continue;
    /// ID AutoInc
    oPrimaryKey := AObject.GetPrimaryKey(oProperty);
    if oPrimaryKey <> nil then
      if oPrimaryKey.SequenceType = AutoInc then
        oProperty.SetValue(AObject, GeneratorSequenceNextValue(AObject, ACommandInsert));
    ///
    oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject, oProperty));
  end;
  Result := oCriteria.AsString;
end;

function TDMLGeneratorAbstract.GeneratorSelect(ASQL: ICriteria; APageSize: Integer): string;
begin
  Result := ASQL.AsString;
end;

function TDMLGeneratorAbstract.GeneratorUpdate(AObject: TObject): string;
begin
  Result := GeneratorUpdate(AObject, nil);
end;

function TDMLGeneratorAbstract.GetGeneratorSelect(ACriteria: ICriteria): string;
begin
  Result := '';
end;

function TDMLGeneratorAbstract.GetCriteriaSelect(AClass: TClass; AID: TValue): ICriteria;
var
  oTable: TTableMapping;
  oColumnList: TColumnMappingList;
  oColumn: TColumnMapping;
  oPrimaryKey: TPrimaryKeyMapping;
  oCriteria: ICriteria;
begin
  oCriteria := CreateCriteria.Select;
  /// Table
  oTable := TMappingExplorer.GetInstance.GetMappingTable(AClass);
  oCriteria.From(oTable.Name);
  /// Columns
  oColumnList := TMappingExplorer.GetInstance.GetMappingColumn(AClass);
  for oColumn in oColumnList do
    oCriteria.Column(oTable.Name + '.' + oColumn.Name);
  /// JoinColumn
  SetJoinColumn(AClass, oTable, oCriteria);
  /// PrimaryKey
  oPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKey(AClass);
  if oPrimaryKey <> nil then
  begin
    if AID.AsInteger > 0 then
      oCriteria.Where(oPrimaryKey.Columns[0] + '=' + IntToStr(AID.AsInteger));
    oCriteria.OrderBy(oPrimaryKey.Columns[0]);
  end;
  Result := oCriteria;
end;

function TDMLGeneratorAbstract.GetPropertyValue(AObject: TObject; AProperty: TRttiProperty): Variant;
begin
  { TODO -oISAQUE : Tratar todos os tipo aqui }
  case TRttiSingleton.GetInstance.GetFieldType(AProperty.PropertyType.Handle) of
     ftString,ftWideString:
     begin
        Result := QuotedStr(AProperty.GetNullableValue(AObject).AsString)
     end;
     ftVariant:
     begin
        Result := VarToStr(AProperty.GetNullableValue(AObject).AsVariant);
     end;
     ftInteger,ftWord,ftSmallint:
     begin
        Result := IntToStr(AProperty.GetNullableValue(AObject).AsInteger);
     end;
     ftFloat:
     begin
        Result := FloatToStr(AProperty.GetNullableValue(AObject).AsExtended);
        Result := ReplaceStr(Result, ',', '.');
     end;
     ftDateTime,ftDate:
     begin
        Result := QuotedStr(FormatDateTime(FDateFormat, AProperty.GetNullableValue(AObject).AsExtended))
     end;
     ftTime,ftTimeStamp:
     begin
        Result := QuotedStr(FormatDateTime(FTimeFormat, AProperty.GetNullableValue(AObject).AsExtended))
     end;
     ftCurrency:
     begin
        Result := CurrToStr(AProperty.GetNullableValue(AObject).AsCurrency);
        Result := ReplaceStr(Result, ',', '.');
     end;
  else
     Result := '';
  end;
end;

function TDMLGeneratorAbstract.IsNull(AProperty: TRttiProperty;
  AObject: TObject): Boolean;
begin
  Result := False;
  if AProperty.PropertyType.TypeKind in [tkUnknown,tkEnumeration,tkClass,tkArray,tkDynArray,tkMethod,
                                         tkPointer,tkSet,tkClassRef,tkProcedure,tkInterface] then
     Exit(True);

  if AProperty.PropertyType.TypeKind in [tkString, tkUString] then
     if AProperty.GetValue(AObject).ToString = 'null' then
        Exit(True);

  if AProperty.PropertyType.TypeKind in [tkFloat] then
     if AProperty.PropertyType.Handle = TypeInfo(TDateTime) then
        if AProperty.GetValue(AObject).AsExtended = 0 then
           Exit(True);

  if AProperty.PropertyType.TypeKind in [tkFloat] then
     if AProperty.PropertyType.Handle = TypeInfo(TTime) then
        if AProperty.GetValue(AObject).AsExtended = 0 then
           Exit(True);
end;

procedure TDMLGeneratorAbstract.SetConnection(const AConnaction: IDBConnection);
begin
  FConnection := AConnaction;
end;

procedure TDMLGeneratorAbstract.SetJoinColumn(AClass: TClass; ATable: TTableMapping; ACriteria: ICriteria);
var
  oJoinList: TJoinColumnMappingList;
  oJoin: TJoinColumnMapping;
  oJoinExist: TList<string>;
begin
  oJoinExist := TList<string>.Create;
  try
    /// JoinColumn
    oJoinList := TMappingExplorer.GetInstance.GetMappingJoinColumn(AClass);
    if oJoinList <> nil then
    begin
      for oJoin in oJoinList do
      begin
        ACriteria.Column(oJoin.ReferencedTableName + '.' + oJoin.ColumnName);
        if oJoinExist.IndexOf(oJoin.ReferencedTableName) = -1 then
        begin
          oJoinExist.Add(oJoin.ReferencedTableName);
          /// Join Inner, Left, Right, Full
          if oJoin.Join = InnerJoin then
            ACriteria.InnerJoin(oJoin.ReferencedTableName).
                      &On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = LeftJoin then
            ACriteria.LeftJoin(oJoin.ReferencedTableName).
                      &On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = RightJoin then
            ACriteria.RightJoin(oJoin.ReferencedTableName).
                      &On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName])
          else
          if oJoin.Join = FullJoin then
            ACriteria.FullJoin(oJoin.ReferencedTableName).
                      &On([oJoin.ReferencedTableName + '.' + oJoin.ReferencedColumnName,'=',ATable.Name + '.' + oJoin.ReferencedColumnName]);
        end;
      end;
    end;
  finally
    oJoinExist.Free;
  end;
end;

function TDMLGeneratorAbstract.GeneratorUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string;
var
  oTable: Table;
  oRestAttr: TCustomAttribute;
  oProperty: TRttiProperty;
  oCriteria: ICriteria;
  oCriteriaWhere: ICriteria;
begin
  oCriteria := CreateCriteria;
  oCriteriaWhere := CreateCriteria;
  oCriteria.Update(AObject.GetTable.Name);
  for oProperty in AObject.GetPrimaryKey do
     oCriteriaWhere.Where(oProperty.Name + '=' + GetPropertyValue(AObject, oProperty));
  for oProperty in AObject.GetColumns do
  begin
    /// <summary>
    /// Restrictions [Required, Unique, Check, NoInsert, NoUpdate, NotNull, Hidden]
    /// </summary>
    oRestAttr := AObject.GetRestriction(oProperty);
    if oRestAttr <> nil then
      if NoUpdate in Restrictions(oRestAttr).Restrictions then
        Continue;
    /// <summary>
    /// Lista de campos modificados
    /// </summary>
    if AModifiedFields <> nil then
    begin
      if AModifiedFields.ContainsKey(oProperty.Name) then
      begin
        { TODO -oISAQUE : No futuro tenho que tratar para comparar o valor
                          do TField com o Valor da Propriedade }
  //            if AModifiedFields.Items[oProperty.Name].OldValue <> AModifiedFields.Items[oProperty.Name].NewValue then
           oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject, oProperty));
      end;
    end
    else
       oCriteria.&Set(oProperty.Name, GetPropertyValue(AObject, oProperty));
  end;
  Result := oCriteria.AsString + ' ' + oCriteriaWhere.AsString
end;

end.
