{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.selecter;

interface

uses
  SysUtils,
  Rtti,
  ormbr.criteria,
  ormbr.command.abstract,
  ormbr.factory.interfaces,
  ormbr.types.database;

type
  TCommandSelecter = class(TDMLCommandAbstract)
  private
    FPageSize: Integer;
    FPageNext: Integer;
    FDMLCommandSelect: string;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName); override;
    procedure SetPageSize(APageSize: Integer);
    function GenerateSelect(ASQL: ICriteria): string;
    function GenerateSelectAll(AClass: TClass): string;
    function GenerateSelectID(AClass: TClass; AID: TValue): string;
    function GenerateNextPacket: string;
  end;

implementation

{ TCommandSelecter }

function TCommandSelecter.GenerateNextPacket;
begin
  FPageNext := FPageNext + FPageSize;
  if FPageSize > -1 then
     FDMLCommand := Format(FDMLCommandSelect, [FPageSize, FPageNext]);
  Result := FDMLCommand;
end;

procedure TCommandSelecter.SetPageSize(APageSize: Integer);
begin
  FPageSize := APageSize;
end;

function TCommandSelecter.GenerateSelect(ASQL: ICriteria): string;
begin
  FPageNext := 0;
  FDMLCommandSelect := FDMLGeneratorCommand.GeneratorSelect(ASQL, FPageSize);
  if FPageSize > -1 then
     FDMLCommand := Format(FDMLCommandSelect, [FPageSize, FPageNext])
  else
     FDMLCommand := FDMLCommandSelect;
  Result := FDMLCommand;
end;

function TCommandSelecter.GenerateSelectAll(AClass: TClass): string;
begin
  FPageNext := 0;
  FDMLCommandSelect := FDMLGeneratorCommand.GeneratorSelectAll(AClass, FPageSize, -1);
  if FPageSize > -1 then
     FDMLCommand := Format(FDMLCommandSelect, [FPageSize, FPageNext])
  else
     FDMLCommand := FDMLCommandSelect;
  Result := FDMLCommand;
end;

function TCommandSelecter.GenerateSelectID(AClass: TClass; AID: TValue): string;
begin
  FPageNext := 0;
  FDMLCommandSelect := FDMLGeneratorCommand.GeneratorSelectAll(AClass, FPageSize, AID);
  if FPageSize > -1 then
     FDMLCommand := Format(FDMLCommandSelect, [FPageSize, FPageNext])
  else
     FDMLCommand := FDMLCommandSelect;
  Result := FDMLCommand;
end;

constructor TCommandSelecter.Create(AConnection: IDBConnection; ADriverName: TDriverName);
begin
  inherited Create(AConnection, ADriverName);
  FDMLCommandSelect := '';
  FDMLCommand := '';
  FPageSize := -1;
  FPageNext := 0;
end;

end.
