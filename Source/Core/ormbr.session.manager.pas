{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.session.manager;

interface

uses
  Classes,
  Generics.Collections,
  SysUtils,
  Windows,
  DB,
  Rtti,
  /// orm
  ormbr.objects.manager,
  ormbr.factory.interfaces,
  ormbr.mapping.explorer,
  ormbr.criteria;

type
  TObjectSetState = (osBrowse, osEdit, osInsert);

  /// <summary>
  /// M - Sess�o Abstract
  /// </summary>
  TSessionAbstract<M: class, constructor> = class abstract
  private
    /// <summary>
    /// Instancia a class que mapea todas as class do tipo Entity
    /// </summary>
    FExplorer: TMappingExplorer;
    /// <summary>
    /// Instancia a class do tipo generics recebida
    /// </summary>
    FManager: TObjectManager<M>;
    /// <summary>
    /// Captura a conex�o passada como par�metro
    /// </summary>
    FFetchingRecords: Boolean;
    /// <summary>
    /// Controle de pagina��o vindo do banco de dados
    /// </summary>
    FPageSize: Integer;
    /// <summary>
    /// Se n�o usar DataSet, preenche a lista de objetos que � usada como cache em mem�ria.
    /// </summary>
    FUseDataSet: Boolean;
    function GetManager: TObjectManager<M>;
  protected
  public
    constructor Create(AConnection: IDBConnection; APageSize: Integer = -1); overload; virtual;
    destructor Destroy; override;
    procedure Close;
    procedure Delete(AObject: TObject);
    procedure Post(AObject: TObject; State: TObjectSetState);
    function Open(ASQL: ICriteria): IDBResultSet; overload;
    function Open(AID: TValue): IDBResultSet; overload;
    function Open: IDBResultSet; overload;
    function GetNextPacket: IDBResultSet;
    function RecordCount: Integer;
    property Manager: TObjectManager<M> read GetManager;
    property Explorer: TMappingExplorer read FExplorer;
    property FetchingRecords: Boolean read FFetchingRecords; // write FFetchingRecords;
    property UseDataSet: Boolean read FUseDataSet write FUseDataSet;
  end;

  /// <summary>
  /// M - Sess�o DataSet
  /// </summary>
  TSessionDataSet<M: class, constructor> = class(TSessionAbstract<M>)
  private
  public
  end;

  /// <summary>
  /// M - Sess�o Objeto
  /// </summary>
  TSessionManager<M: class, constructor> = class(TSessionAbstract<M>)
  private
  public
  end;

implementation

{ TSessionAbstract<M> }

procedure TSessionAbstract<M>.Close;
begin
  FManager.Clear;
end;

constructor TSessionAbstract<M>.Create(AConnection: IDBConnection; APageSize: Integer);
begin
  FPageSize := APageSize;
  FUseDataSet := False;
  FManager := TObjectManager<M>.Create(AConnection, AConnection.GetDriverName);
  FExplorer := TMappingExplorer.GetInstance;
end;

procedure TSessionAbstract<M>.Delete(AObject: TObject);
begin
  FManager.DeleteCommand(AObject);
end;

destructor TSessionAbstract<M>.Destroy;
begin
  FManager.Free;
  FExplorer := nil;
  inherited;
end;

function TSessionAbstract<M>.GetManager: TObjectManager<M>;
begin
  Result := FManager;
end;

function TSessionAbstract<M>.GetNextPacket: IDBResultSet;
begin
  if not FFetchingRecords then
  begin
    Result := FManager.GetNextPacketCommand;
    /// <summary>
    /// Se o DataSet n�o estiver em uso, preenche a lista de objetos
    /// </summary>
    if not FUseDataSet then
      FManager.Fill(Result);
    if Result.FetchingAll then
      FFetchingRecords := True;
  end;
end;

function TSessionAbstract<M>.Open(AID: TValue): IDBResultSet;
begin
  FFetchingRecords := False;
  Result := FManager.SelectCommandID(AID, -1);
  /// <summary>
  /// Se o DataSet n�o estiver em uso, preenche a lista de objetos
  /// </summary>
  if not FUseDataSet then
    FManager.Fill(Result);
end;

function TSessionAbstract<M>.Open(ASQL: ICriteria): IDBResultSet;
begin
  FFetchingRecords := False;
  if ASQL.AsString = '' then
    Result := FManager.SelectCommandAll(FPageSize)
  else
    Result := FManager.SelectCommand(ASQL, FPageSize);
  /// <summary>
  /// Se o DataSet n�o estiver em uso, preenche a lista de objetos
  /// </summary>
  if not FUseDataSet then
    FManager.Fill(Result);
end;

function TSessionAbstract<M>.Open: IDBResultSet;
begin
  FFetchingRecords := False;
  Result := FManager.SelectCommandAll(FPageSize);
  /// <summary>
  /// Se o DataSet n�o estiver em uso, preenche a lista de objetos
  /// </summary>
  if not FUseDataSet then
    FManager.Fill(Result);
end;

procedure TSessionAbstract<M>.Post(AObject: TObject; State: TObjectSetState);
begin
  if State in [osInsert] then
    FManager.InsertCommand(AObject)
  else
  if State in [osEdit] then
    FManager.UpdateCommand(AObject);
end;

function TSessionAbstract<M>.RecordCount: Integer;
begin
  Result := FManager.List.Count;
end;

end.


