{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(12 Out 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.dml.interfaces;

interface

uses
  DB,
  Rtti,
  Generics.Collections,
  /// ormbr
  ormbr.factory.interfaces,
  ormbr.dml.commands,
  ormbr.criteria;

type
  IDMLGeneratorCommand = interface
    ['{8B569AB2-FD86-4D72-B5AC-6CCA3C6FC52A}']
    procedure SetConnection(const AConnaction: IDBConnection);
    function GeneratorSelect(ASQL: ICriteria; APageSize: Integer): string;
    function GeneratorSelectAll(AClass: TClass; APageSize: Integer; AID: TValue): string;
    function GeneratorUpdate(AObject: TObject): string; overload;
    function GeneratorUpdate(AObject: TObject; AModifiedFields: TDictionary<string, TField>): string; overload;
    function GeneratorInsert(AObject: TObject; ACommandInsert: TDMLCommandInsert): string;
    function GeneratorDelete(AObject: TObject): string;
    function GeneratorSequenceCurrentValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64;
    function GeneratorSequenceNextValue(AObject: TObject; ACommandInsert: TDMLCommandInsert): Int64;
  end;

implementation

end.
