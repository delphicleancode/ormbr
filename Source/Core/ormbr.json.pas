{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

{$INCLUDE ..\ormbr.inc}

unit ormbr.json;

interface

uses
  Generics.Collections,
  {$IFDEF D2010}
  DBXJSON,
  DBXJSONReflect
  {$ELSE}
  System.JSON,
  REST.JsonReflect
  {$ENDIF D2010};

type
  TJson = class(TObject)
  private
  public
    class function Format(AJsonValue: TJsonValue): string; overload;
    class function Format(AJsonString: string): string; overload;
    class function JsonEncode(AJsonValue: TJsonValue): string; overload;
    class function JsonEncode(AJsonString: string): string; overload;
    class function JsonEncodeUTF7(AJsonString: string): TJsonObject; overload;
    class function JsonEncodeUTF8(AJsonString: string): TJsonObject; overload;
    {$IFNDEF D2010}
    class function JsonEncodeANSI(AJsonString: string): TJsonObject; overload;
    {$ENDIF D2010}
    class function JsonEncodeASCII(AJsonString: string): TJsonObject; overload;

    class function ObjectToJson<T: class>(AObject: T): TJsonValue;
    class function JsonToObject<T: class, constructor>(AJsonValue: TJsonObject): T; overload;
    class function JsonToObject<T: class, constructor>(AJsonString: string): T; overload;
    class function ObjectToJsonString<T: class>(AObject: TObject): string;
    class function JsonArrayToList<T: class, constructor>(AJsonArray: TJsonArray): TList<T>;
    class function ListToJsonArray<T: class, constructor>(AList: TList<T>): TJsonArray;
  end;

implementation

uses
  DateUtils,
  SysUtils,
  Rtti,
  Character;

class function TJson.Format(AJsonValue: TJsonValue): string;
begin
  Result := Format(AJsonValue.ToString);
end;

class function TJson.JsonEncode(AJsonValue: TJsonValue): string;
var
  LBytes: TBytes;
begin
  SetLength(LBytes, Length(AJsonValue.ToString) * 6);
  SetLength(LBytes, AJsonValue.ToBytes(LBytes, 0));
  Result := TEncoding.UTF8.GetString(LBytes);
end;

class function TJson.Format(AJsonString: string): string;
var
  s: string;
  c: char;
  EOL: string;
  INDENT: string;
  LIndent: string;
  isEOL: boolean;
  isInString: boolean;
  isEscape: boolean;
begin
  EOL := #13#10;
  INDENT := '  ';
  isEOL := true;
  isInString := false;
  isEscape := false;
  s := AJsonString;
  for c in s do
  begin
    if not isInString and ((c = '{') or (c = '[')) then
    begin
      if not isEOL then
        Result := Result + EOL;
      Result := Result + LIndent + c + EOL;
      LIndent := LIndent + INDENT;
      Result := Result + LIndent;
      isEOL := true;
    end
    else if not isInString and (c = ',') then
    begin
      isEOL := false;
      Result := Result + c + EOL + LIndent;
    end
    else if not isInString and ((c = '}') or (c = ']')) then
    begin
      Delete(LIndent, 1, Length(INDENT));
      if not isEOL then
        Result := Result + EOL;
      Result := Result + LIndent + c + EOL;
      isEOL := true;
    end
    else
    begin
      isEOL := false;
      Result := Result + c;
    end;
    isEscape := (c = '\') and not isEscape;
    if not isEscape and (c = '"') then
      isInString := not isInString;
  end;
end;

class function TJson.JsonEncode(AJsonString: string): string;
var
  LJsonValue: TJsonValue;
begin
  {$IFDEF D2010}
  LJsonValue := JsonEncodeUTF8(AJsonString);
  {$ELSE}
  LJsonValue := TJsonObject.ParseJsonValue(AJsonString);
  {$ENDIF D2010}
  try
    Result := JsonEncode(LJsonValue);
  finally
    LJsonValue.Free;
  end;
end;

{$IFNDEF D2010}
class function TJson.JsonEncodeANSI(AJsonString: string): TJsonObject;
begin
  Result := TJsonObject.ParseJsonValue(TEncoding.ANSI.GetBytes(AJsonString), 0) as TJsonObject;
end;
{$ENDIF D2010}

class function TJson.JsonEncodeASCII(AJsonString: string): TJsonObject;
begin
  Result := TJsonObject.ParseJsonValue(TEncoding.ASCII.GetBytes(AJsonString), 0) as TJsonObject;
end;

class function TJson.JsonEncodeUTF7(AJsonString: string): TJsonObject;
begin
  Result := TJsonObject.ParseJsonValue(TEncoding.UTF7.GetBytes(AJsonString), 0) as TJsonObject;
end;

class function TJson.JsonEncodeUTF8(AJsonString: string): TJsonObject;
begin
  Result := TJsonObject.ParseJsonValue(TEncoding.UTF8.GetBytes(AJsonString), 0) as TJsonObject;
end;

class function TJson.JsonToObject<T>(AJsonString: string): T;
begin
  Result := JsonToObject<T>(JsonEncodeUTF8(AJsonString));
end;

class function TJson.ObjectToJson<T>(AObject: T): TJsonValue;
var
  oMarshal: TJsonMarshal;
begin
  if Assigned(AObject) then
  begin
    oMarshal := TJsonMarshal.Create(TJsonConverter.Create);
    try
      Exit(oMarshal.Marshal(AObject));
    finally
      oMarshal.Free;
    end;
  end
  else
    Exit(TJsonNull.Create);
end;

class function TJson.ObjectToJsonString<T>(AObject: TObject): string;
begin
  Result := TJson.JsonEncode(ObjectToJson<T>(AObject).ToString);
end;

class function TJson.JsonToObject<T>(AJsonValue: TJsonObject): T;
var
  oUnMarshal: TJsonUnMarshal;
begin
  if AJsonValue.Null then
    Exit(nil);
  oUnMarshal := TJsonUnMarshal.Create;
  try
    {$IFDEF D2010}
    Exit(T(oUnMarshal.Unmarshal(AJsonValue)))
    {$ELSE}
    Exit(oUnMarshal.CreateObject(T, AJsonValue) as T)
    {$ENDIF D2010}
  finally
    oUnMarshal.Free;
  end;
end;

class function TJson.JsonArrayToList<T>(AJsonArray: TJsonArray): TList<T>;
var
  iFor: Integer;
begin
  Result := TList<T>.Create;
  for iFor := 0 to AJsonArray.Size - 1 do
    Result.Add(JsonToObject<T>(TJsonObject(AJsonArray.Get(iFor))));
end;

class function TJson.ListToJsonArray<T>(AList: TList<T>): TJsonArray;
var
  iFor: Integer;
begin
  Result := TJsonArray.Create;
  for iFor := 0 to AList.Count - 1 do
    Result.AddElement(ObjectToJson(AList[iFor]));
end;

end.
