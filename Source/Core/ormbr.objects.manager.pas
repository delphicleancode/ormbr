{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.objects.manager;

interface

uses
  DB,
  Rtti,
  Types,
  Classes,
  SysUtils,
  Generics.Collections,
  /// ormbr
  ormbr.criteria,
  ormbr.command.factory,
  ormbr.factory.interfaces,
  ormbr.types.database,
  ormbr.bind.objects,
  ormbr.driver.register;

type
  TObjectManager<M: class, constructor> = class abstract
  private
    FCurrentRecord: Integer;
    procedure DoNotifyList(Sender: TObject; const Item: M; Action: TCollectionNotification);
    function GetCurrentRecord: M;
  protected
    FConnection: IDBConnection;
    FObjectList: TObjectList<M>;
    FDMLCommandFactory: TDMLCommandFactory;
  public
    constructor Create(const AConnection: IDBConnection; const ADriverName: TDriverName); virtual;
    destructor Destroy; override;
    procedure InsertCommand(AObject: TObject); virtual;
    procedure UpdateCommand(AObject: TObject); overload; virtual;
    procedure UpdateCommand(AObject: TObject; AModifiedFields: TDictionary<string, TField>); overload; virtual;
    procedure DeleteCommand(AObject: TObject); virtual;
    procedure Fill(ADBResultSet: IDBResultSet); virtual;
    procedure Clear;
    function SelectCommandAll(APageSize: Integer): IDBResultSet; virtual;
    function SelectCommandID(AID: TValue; APageSize: Integer): IDBResultSet; virtual;
    function SelectCommand(ASQL: ICriteria; APageSize: Integer): IDBResultSet; virtual;
    function GetDMLCommand: string;
    function GetNextPacketCommand: IDBResultSet; virtual;
    function Add: M; virtual;
    function Find(AIndex: Integer): Boolean;
    function ExistSequence: Boolean;
//    function GetSequenceCurrentValue(AClass: TClass): Int64;
//    function GetSequenceNextValue(AClass: TClass): Int64;
    /// <summary>
    /// Uso na interface para ler, gravar e alterar dados do registro atual do dataset, pelo objeto.
    /// </summary>
    property List: TObjectList<M> read FObjectList;
    property Current: M read GetCurrentRecord;
  end;

implementation

{ TObjectManager<M> }

procedure TObjectManager<M>.Clear;
begin
  FObjectList.Clear;
end;

constructor TObjectManager<M>.Create(const AConnection: IDBConnection;
  const ADriverName: TDriverName);
begin
  FConnection := AConnection;
  /// Instancia a lista de Objects do tipo
  FObjectList := TObjectList<M>.Create;
  FDMLCommandFactory := TDMLCommandFactory.Create(TObject(M), AConnection, ADriverName);
  FObjectList.OnNotify := DoNotifyList;
end;

destructor TObjectManager<M>.Destroy;
begin
  FDMLCommandFactory.Free;
  FObjectList.Clear;
  FObjectList.Free;
  inherited;
end;

function TObjectManager<M>.Add: M;
begin
  Result := FObjectList.Items[FObjectList.Add(M.Create)];
end;

procedure TObjectManager<M>.DeleteCommand(AObject: TObject);
begin
  FDMLCommandFactory.GeneratorDelete(AObject);
end;

function TObjectManager<M>.SelectCommandAll(APageSize: Integer): IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelectAll(M, APageSize);
end;

function TObjectManager<M>.SelectCommandID(AID: TValue; APageSize: Integer): IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelectID(M, AID, APageSize);
end;

function TObjectManager<M>.GetCurrentRecord: M;
begin
  Result := FObjectList.Items[FCurrentRecord];
end;

function TObjectManager<M>.GetNextPacketCommand: IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorNextPacket;
end;

function TObjectManager<M>.ExistSequence: Boolean;
begin
  Result := FDMLCommandFactory.ExistSequence;
end;

//function TObjectManager<M>.GetSequenceCurrentValue(AClass: TClass): Int64;
//begin
//  Result := FDMLCommandFactory.GeneratorSequenceCurrentValue(AClass);
//end;

//function TObjectManager<M>.GetSequenceNextValue(AClass: TClass): Int64;
//begin
//  Result := FDMLCommandFactory.GeneratorSequenceNextValue(AClass);
//end;

function TObjectManager<M>.GetDMLCommand: string;
begin
  Result := FDMLCommandFactory.GetDMLCommand;
end;

function TObjectManager<M>.SelectCommand(ASQL: ICriteria; APageSize: Integer): IDBResultSet;
begin
  Result := FDMLCommandFactory.GeneratorSelect(ASQL, APageSize);
end;

procedure TObjectManager<M>.UpdateCommand(AObject: TObject);
begin
  FDMLCommandFactory.GeneratorUpdate(AObject);
end;

procedure TObjectManager<M>.UpdateCommand(AObject: TObject;
  AModifiedFields: TDictionary<string, TField>);
begin
  FDMLCommandFactory.GeneratorUpdate(AObject, AModifiedFields);
end;

procedure TObjectManager<M>.InsertCommand(AObject: TObject);
begin
  FDMLCommandFactory.GeneratorInsert(AObject);
end;

procedure TObjectManager<M>.DoNotifyList(Sender: TObject; const Item: M;
  Action: TCollectionNotification);
begin
   case Action of
     cnAdded:
     begin
//        OutputDebugString(PChar('cnAdded'));
     end;
     cnRemoved:
     begin
//        OutputDebugString(PChar('cnRemoved'));
     end;
     cnExtracted:
     begin
//        OutputDebugString(PChar('cnExtracted'));
     end;
   end;
end;

procedure TObjectManager<M>.Fill(ADBResultSet: IDBResultSet);
begin
  /// <summary>
  /// Adiciona um objeto M para cada registro e popula suas propriedades com
  /// os dados do registro do DBResultSet.
  /// </summary>
  while ADBResultSet.NotEof do
    TBindObject.GetInstance.SetFieldToProperty(ADBResultSet, Add);

  /// <summary>
  /// � atribuido a FCurrentRecord o 1o objeto da lista.
  /// </summary>
  if FObjectList.Count > 0 then
     FCurrentRecord := 0;
end;

function TObjectManager<M>.Find(AIndex: Integer): Boolean;
begin
  Result := False;
  if FObjectList.Count > 0 then
  begin
    FCurrentRecord := AIndex;
    Result := True;
  end;
end;

end.
