{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.inserter;

interface

uses
  ormbr.command.abstract,
  ormbr.factory.interfaces,
  ormbr.types.database,
  ormbr.dml.commands,
  ormbr.objects.helper;

type
  TCommandInserter = class(TDMLCommandAbstract)
  private
   FDMLCommandInsert: TDMLCommandInsert;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName); override;
    destructor Destroy; override;
    function GenerateInsert(AObject: TObject): string;
//    function GeneratorSequenceCurrentValue(AObject: TObject): string;
//    function GeneratorSequenceNextValue(AObject: TObject): string;
    property Sequence: TDMLCommandInsert read FDMLCommandInsert;
  end;

implementation

{ TCommandInserter }

destructor TCommandInserter.Destroy;
begin
  if Assigned(FDMLCommandInsert) then
    FDMLCommandInsert.Free;
  inherited;
end;

function TCommandInserter.GenerateInsert(AObject: TObject): string;
begin
  if not Assigned(FDMLCommandInsert) then
  begin
    FDMLCommandInsert := TDMLCommandInsert.Create;
    FDMLCommandInsert.Table := AObject.GetTable;
    FDMLCommandInsert.Sequence := AObject.GetSequence;
    if FDMLCommandInsert.Sequence <> nil then
      FDMLCommandInsert.ExistSequence := True
    else
      FDMLCommandInsert.ExistSequence := False;
  end;
  FDMLCommand := FDMLGeneratorCommand.GeneratorInsert(AObject, FDMLCommandInsert);
  Result := FDMLCommand;
end;

//function TCommandInserter.GeneratorSequenceCurrentValue(AObject: TObject): string;
//begin
//  FDMLCommand := FDMLGeneratorCommand.GeneratorSequenceCurrentValue(AObject, FDMLCommandInsert);
//  Result := FDMLCommand;
//end;

//function TCommandInserter.GeneratorSequenceNextValue(AObject: TObject): string;
//begin
//  FDMLCommand := FDMLGeneratorCommand.GeneratorSequenceNextValue(AObject, FDMLCommandInsert);
//  Result := FDMLCommand;
//end;

constructor TCommandInserter.Create(AConnection: IDBConnection; ADriverName: TDriverName);
begin
  inherited Create(AConnection, ADriverName);
end;

end.
