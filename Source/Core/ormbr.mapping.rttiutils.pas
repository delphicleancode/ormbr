{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.mapping.rttiutils;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  TypInfo,
  Math,
  StrUtils,
  Types,
  Variants,
  Generics.Collections,
  /// orm
  ormbr.mapping.attributes,
  ormbr.types.mapping;

type
  TStrArray = array of String;

  TRttiSingleton = class
  private
  class var
    FInstance: TRttiSingleton;
  private
    FContext: TRttiContext;
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    class function GetInstance: TRttiSingleton;

    function GetRuleAction(ARuleAction: string): TRuleAction; overload;
    function GetRuleAction(ARuleAction: Variant): TRuleAction; overload;
    function GetRttiType(AClass: TClass): TRttiType;
    function GetListType(RttiType: TRttiType): TRttiType;
    function GetFieldType(ATypeInfo: PTypeInfo): TFieldType;
    function TryGetUnderlyingTypeInfo(TypeInfo: PTypeInfo; out UnderlyingTypeInfo: PTypeInfo): Boolean;

    function IsNullable(TypeInfo: PTypeInfo): Boolean;
    function IsLazy(RttiType: TRttiType): Boolean;
    function RunValidade(AClass: TClass): Boolean;
    function MethodCall(AObject: TObject; AMethodName: string; const AParameters: array of TValue): TValue;
    function Explode(var AFields: TStrArray; ADelimiter, ATexto: String): Integer;
  end;

implementation

{ TRttiSingleton }

constructor TRttiSingleton.Create;
begin
   raise Exception.Create('Para usar o MappingEntity use o m�todo TRttiSingleton.GetInstance()');
end;

constructor TRttiSingleton.CreatePrivate;
begin
   inherited;
   FContext := TRttiContext.Create;
end;

destructor TRttiSingleton.Destroy;
begin
  FContext.Free;
  inherited;
end;

function TRttiSingleton.GetRttiType(AClass: TClass): TRttiType;
begin
  Result := FContext.GetType(AClass);
end;

function TRttiSingleton.GetRuleAction(ARuleAction: Variant): TRuleAction;
begin
  if      ARuleAction = 0 then Result := None
  else if ARuleAction = 1 then Result := Cascade
  else if ARuleAction = 2 then Result := SetNull
  else if ARuleAction = 3 then Result := SetDefault
  else Result := None;
end;

function TRttiSingleton.GetRuleAction(ARuleAction: string): TRuleAction;
begin
  if      ARuleAction = 'NO ACTION'   then Result := None
  else if ARuleAction = 'SET NULL'    then Result := SetNull
  else if ARuleAction = 'SET DEFAULT' then Result := SetDefault
  else if ARuleAction = 'CASCADE'     then Result := Cascade
  else Result := None;
end;

function TRttiSingleton.GetFieldType(ATypeInfo: PTypeInfo): TFieldType;
var
  oTypeInfo: PTypeInfo;
begin
   Result := ftUnknown;
   case ATypeInfo.Kind of
     tkInteger:
     begin
       if ATypeInfo = TypeInfo(Word) then
          Result := ftWord
       else
       if ATypeInfo = TypeInfo(SmallInt) then
          Result := ftSmallint
       else
          Result := ftInteger;
     end;
     tkEnumeration:
     begin
       if ATypeInfo = TypeInfo(Boolean) then
          Result := ftBoolean
       else
          Result := ftWideString;
     end;
     tkFloat:
     begin
       if ATypeInfo = TypeInfo(TDate) then
          Result := ftDate
       else
       if ATypeInfo = TypeInfo(TDateTime) then
          Result := ftDateTime
       else
       if ATypeInfo = TypeInfo(Currency) then
          Result := ftCurrency
       else
       if ATypeInfo = TypeInfo(TTime) then
          Result := ftTime
       else
          Result := ftFloat;
     end;
     tkString, tkLString, tkChar:
        Result := ftString;
     tkVariant, tkArray, tkDynArray:
        Result := ftVariant;
     tkClass:
     begin
       if TypeInfo(TStringStream) = ATypeInfo then
          Result := ftMemo
       else
          Result := ftBlob;
     end;
     tkRecord:
     begin
       if IsNullable(ATypeInfo) then
       begin
          TryGetUnderlyingTypeInfo(ATypeInfo, oTypeInfo);
          Result := GetFieldType(oTypeInfo);
       end;
     end;
     tkInt64:
       Result := ftLargeint;
     tkUString, tkWString, tkWChar, tkSet:
       Result := ftWideString;
   end;
end;

function TRttiSingleton.TryGetUnderlyingTypeInfo(TypeInfo: PTypeInfo; out UnderlyingTypeInfo: PTypeInfo): Boolean;
var
  Context: TRttiContext;
  RttiType: TRttiType;
  ValueField: TRttiField;
begin
  Result := IsNullable(TypeInfo);
  if Result then
  begin
    RttiType := Context.GetType(TypeInfo);
    ValueField := RttiType.GetField('FValue');
    Result := Assigned(ValueField);
    if Result then
       UnderlyingTypeInfo := ValueField.FieldType.Handle
    else
       UnderlyingTypeInfo := nil;
  end;
end;

class function TRttiSingleton.GetInstance: TRttiSingleton;
begin
   if not Assigned(FInstance) then
      FInstance := TRttiSingleton.CreatePrivate;

   Result := FInstance;
end;

function TRttiSingleton.GetListType(RttiType: TRttiType): TRttiType;
var
  sTypeName: string;
  oContext: TRttiContext;
begin
   oContext := TRttiContext.Create;
   try
     sTypeName := RttiType.ToString;
     sTypeName := StringReplace(sTypeName,'TObjectList<','',[]);
     sTypeName := StringReplace(sTypeName,'TList<','',[]);
     sTypeName := StringReplace(sTypeName,'>','',[]);
     ///
     Result := oContext.FindType(sTypeName);
   finally
     oContext.Free;
   end;
end;

function TRttiSingleton.RunValidade(AClass: TClass): Boolean;
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oPropertyAttr: TCustomAttribute;
begin
  Result := False;
  oRttiType := GetRttiType(AClass);
  for oProperty in oRttiType.GetProperties  do
    for oPropertyAttr in oProperty.GetAttributes do
    begin
      if oPropertyAttr is NotNullConstraint then // NotNullConstraint
        NotNullConstraint(oPropertyAttr).Validate(oProperty.Name, oProperty.GetValue(AClass));
      if oPropertyAttr is ZeroConstraint then // ZeroConstraint
        ZeroConstraint(oPropertyAttr).Validate(oProperty.Name, oProperty.GetValue(AClass));
    end;
  Result := True;
end;

function TRttiSingleton.MethodCall(AObject: TObject; AMethodName: string; const AParameters: array of TValue): TValue;
var
  oRttiType: TRttiType;
  oMethod: TRttiMethod;
begin
  oRttiType := GetRttiType(AObject.ClassType);
  oMethod   := oRttiType.GetMethod(AMethodName);
  if Assigned(oMethod) then
     Result := oMethod.Invoke(AObject, AParameters)
  else
     raise Exception.CreateFmt('Cannot find method "%s" in the object',[AMethodName]);
end;

function TRttiSingleton.IsNullable(TypeInfo: PTypeInfo): Boolean;
const
  PrefixString = 'Nullable<';
begin
  Result := Assigned(TypeInfo) and (typeInfo.Kind = tkRecord) and StartsText(PrefixString, GetTypeName(TypeInfo));
end;

function TRttiSingleton.IsLazy(RttiType: TRttiType): boolean;
begin
  Result := (RttiType is TRttiRecordType) and (Pos('Lazy', RttiType.Name) > 0);
end;

function TRttiSingleton.Explode(var AFields: TStrArray; ADelimiter, ATexto: String): Integer;
var
  sTexto: String;
begin
  Result := 0;
  sTexto := ATexto + ADelimiter;
  if Copy(sTexto, 1, 1) = ADelimiter then
    Delete(sTexto, 1, 1);
  repeat
    SetLength(AFields, Length(AFields) + 1);
    AFields[Result] := Copy(sTexto, 0, Pos(ADelimiter, sTexto) - 1);
    Delete(sTexto, 1, Length(AFields[Result] + ADelimiter));
    Inc(Result);
  until (sTexto = '');
end;

initialization

finalization
   if Assigned(TRttiSingleton.FInstance) then
   begin
      TRttiSingleton.FInstance.Free;
   end;
end.

