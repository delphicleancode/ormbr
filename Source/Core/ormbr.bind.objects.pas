{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.bind.objects;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  TypInfo,
  Variants,
  /// orm
  ormbr.mapping.rttiutils,
  ormbr.factory.interfaces,
  ormbr.rtti.helper,
  ormbr.objects.helper;

type
  TBindObject = class
  private
  class var
    FInstance: TBindObject;
    FContext: TRttiContext;
  private
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    class function GetInstance: TBindObject;
    procedure SetFieldToProperty(AResultSet: IDBResultSet; AObject: TObject);
  end;

implementation

{ TBindObject }

constructor TBindObject.Create;
begin
   raise Exception.Create('Para usar o MappingEntity use o m�todo TBindObject.GetInstance()');
end;

constructor TBindObject.CreatePrivate;
begin
   inherited;
   FContext := TRttiContext.Create;
end;

class function TBindObject.GetInstance: TBindObject;
begin
   if not Assigned(FInstance) then
      FInstance := TBindObject.CreatePrivate;

   Result := FInstance;
end;

procedure TBindObject.SetFieldToProperty(AResultSet: IDBResultSet; AObject: TObject);
var
  oProperty: TRttiProperty;
  iFieldIndex: Integer;
begin
   for oProperty in AObject.GetColumns do
   begin
     iFieldIndex := (oProperty as TRttiInstanceProperty).Index;
     if oProperty.PropertyType.TypeKind in [tkString, tkUString] then
        oProperty.SetValue(AObject, String(AResultSet.GetFieldValue(iFieldIndex)))
     else
     if oProperty.PropertyType.TypeKind in [tkFloat] then
     begin
       if oProperty.PropertyType.Handle = TypeInfo(TDateTime) then
          oProperty.SetValue(AObject, TDateTime(AResultSet.GetFieldValue(iFieldIndex)))
       else
       if oProperty.PropertyType.Handle = TypeInfo(TTime) then
          oProperty.SetValue(AObject, TDateTime(AResultSet.GetFieldValue(iFieldIndex)))
       else
          oProperty.SetValue(AObject, Currency(AResultSet.GetFieldValue(iFieldIndex)))
     end
     else
     if oProperty.PropertyType.TypeKind in [tkInteger] then
        oProperty.SetValue(AObject, Integer(AResultSet.GetFieldValue(iFieldIndex)))
     else
     if oProperty.PropertyType.TypeKind in [tkRecord] then
     begin
        if TRttiSingleton.GetInstance.IsNullable(oProperty.PropertyType.Handle) then
           oProperty.SetNullableValue(AObject, oProperty.PropertyType.Handle, AResultSet.GetFieldValue(iFieldIndex));
     end
   end;
end;

initialization

finalization
   if Assigned(TBindObject.FInstance) then
   begin
      TBindObject.FInstance.Free;
      TBindObject.FContext.Free;
   end;
end.

