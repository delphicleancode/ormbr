{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

{$IFDEF VER210}
  {$DEFINE D2010}
{$ENDIF}

{$IFDEF VER220}
  {$DEFINE DXE}
{$ENDIF}

{$IFDEF VER230}
  {$DEFINE DXE2}
{$ENDIF}

{$IFDEF VER240}
  {$DEFINE DXE3}
{$ENDIF}

{$IFDEF VER250}
  {$DEFINE DXE4}
{$ENDIF}

{$IFDEF VER260}
  {$DEFINE DXE5}
{$ENDIF}

{$IFDEF VER270}
  {$DEFINE DXE6}
{$ENDIF}

{$IFDEF VER280}
  {$DEFINE DXE7}
{$ENDIF}

{$IFDEF VER290}
  {$DEFINE DXE8}
{$ENDIF}

{$IFDEF VER300}
  {$DEFINE DSEATTLE}
{$ENDIF}

{$IFDEF VER310}
  {$DEFINE DBERLIN}
{$ENDIF}