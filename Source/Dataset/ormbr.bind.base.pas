{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.bind.base;

interface

uses
  Classes,
  SysUtils,
  StrUtils,
  Generics.Collections,
  Windows,
  DB,
  Rtti,
  TypInfo,
  ormbr.criteria,
  /// orm
  ormbr.objects.manager,
  ormbr.bind.dataset,
  ormbr.bind.events,
  ormbr.session.manager,
  ormbr.factory.interfaces,
  ormbr.mapping.classes,
  ormbr.mapping.attributes,
  ormbr.types.mapping;

type
  /// <summary>
  /// M - Object M
  /// </summary>
  TDataSetBase<M: class, constructor> = class abstract
  private
    /// <summary>
    /// Objeto interface com o DataSet passado pela interface.
    /// </summary>
    FOrmDataSet: TDataSet;
    /// <summary>
    /// Objeto para captura dos eventos do dataset passado pela interface
    /// </summary>
    FOrmDataSetEvents: TDataSet;
    /// <summary>
    /// Objeto para controle de estado do registro
    /// </summary>
    FOrmDataSource: TDataSource;
    /// <summary>
    /// Usado em relacionamento mestre-detalhe, guarda qual objeto pai
    /// </summary>
    FOwnerMasterObject: TObject;
    /// <summary>
    /// Uso interno para captura real do tipo generico passado.
    /// </summary>
//    FClassType: M;
    /// <summary>
    /// Controle de pagina��o vindo do banco de dados
    /// </summary>
    FPageSize: Integer;
    /// <summary>
    /// Classe para controle de evento interno com os eventos da interface do dataset
    /// </summary>
    FDataSetEvents: TDataSetEvents;
    ///
    procedure OpenDataSetChilds;
    procedure SetMasterObject(const AValue: TObject);
    procedure DoStateChange(Sender: TObject);
    procedure DoDataChange(Sender: TObject; Field: TField);
    procedure DoUpdateData(Sender: TObject);
    procedure FillMastersClass(ADatasetBase: TDataSetBase<M>; AObject: M);
    procedure ExecuteOneToOne(AObject: M; oProperty: TRttiProperty; ADatasetBase: TDataSetBase<M>);
    procedure ExecuteOneToMany(AObject: M; oProperty: TRttiProperty; ADatasetBase: TDataSetBase<M>; oRttiType: TRttiType);
    procedure GetMasterValues;
    ///
    function GetCurrentRecordInternal: M;
    function GetNextPacket: Boolean;
    function GetOneToOneRelationFields(ATable: TTableMapping; ADetail: TDataSetBase<M>; var ASQLBuild: ICriteria): Boolean;
    function GetOneToManyRelationFields(ADetail: TDataSetBase<M>; var ASQLBuild: ICriteria): Boolean;
    function FindEvents(AEventName: string): Boolean;
  protected
    /// <summary>
    /// Uso interno para fazer mapeamento do registro dataset
    /// </summary>
    FCurrentRecordInternal: M;
    /// <summary>
    /// Uso na interface, manuseando dados pelo object modelo.
    /// </summary>
    FCurrentRecord: M;
    FModifiedFields: TDictionary<string, TField>;
    FMasterObject: TDictionary<string, TDataSetBase<M>>;
    FLookupsField: TList<TDataSetBase<M>>;
    FSession: TSessionDataSet<M>;
    FInternalIndex: Integer;
    FConnection: IDBConnection;
    procedure DoBeforeScroll(DataSet: TDataSet); virtual;
    procedure DoAfterScroll(DataSet: TDataSet); virtual;
    procedure DoBeforeClose(DataSet: TDataSet); virtual;
    procedure DoAfterClose(DataSet: TDataSet); virtual;
    procedure DoBeforeDelete(DataSet: TDataSet); virtual;
    procedure DoAfterDelete(DataSet: TDataSet); virtual;
    procedure DoBeforePost(DataSet: TDataSet); virtual;
    procedure DoAfterPost(DataSet: TDataSet); virtual;
    procedure DoBeforeCancel(DataSet: TDataSet); virtual;
    procedure DoAfterCancel(DataSet: TDataSet); virtual;
    procedure DoNewRecord(DataSet: TDataSet); virtual;
    procedure EmptyDataSetChilds; virtual; abstract;
    procedure GetDataSetEvents; virtual;
    procedure SetDataSetEvents; virtual;
    procedure DisableDataSetEvents;
    procedure EnableDataSetEvents;
    procedure ApplyInserter(MaxErros: Integer); virtual; abstract;
    procedure ApplyUpdater(MaxErros: Integer); virtual; abstract;
    procedure ApplyDeleter(MaxErros: Integer); virtual; abstract;
    procedure ApplyInternal(MaxErros: Integer); virtual; abstract;
    procedure RefreshRecord;
    property CurrentRecordInternal: M read GetCurrentRecordInternal;
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject); virtual;
    destructor Destroy; override;
    /// Lista
    procedure FillList; virtual;
    procedure FillDataSet; virtual;
    /// Comandos DataSet
    procedure Lazy(AOwner: TObject);
    procedure Open(ASQL: ICriteria; AID: TValue); overload; virtual;
    procedure Open(ASQL: ICriteria); overload; virtual;
    procedure Open(AID: TValue); overload; virtual;
    procedure Open; overload; virtual;
    procedure Append; virtual;
    procedure Post; virtual;
    procedure Edit; virtual;
    procedure Delete; virtual;
    procedure Close; virtual;
    procedure CancelUpdates; virtual;
    procedure ApplyUpdates(MaxErros: Integer); virtual; abstract;
    procedure AddCalcField(AFieldName: String;
                           AFieldType: TFieldType;
                           ASize: Integer = 0;
                           ADisplayFormat: string = '');
    procedure AddLookupField(AFieldName: string;
                             AKeyFields: string;
                             ALookupDataSet: TObject;
                             ALookupKeyFields: string;
                             ALookupResultField: string);
    procedure AddAggregateField(AFieldName: string;
                                AExpression: string;
                                AAlignment: TAlignment = taLeftJustify;
                                ADisplayFormat: string = '');
    function Find: Boolean;
    /// <summary>
    /// Uso na interface para ler, gravar e alterar dados do registro atual do dataset, pelo objeto.
    /// </summary>
    property Current: M read FCurrentRecord;
    /// <summary>
    /// Propriedade para ter acesso a camada Session e seus recursos
    /// </summary>
    property Session: TSessionDataSet<M> read FSession;
  end;

implementation

uses
  ormbr.mapping.rttiutils,
  ormbr.bind.fields,
  ormbr.mapping.explorer;

{ TDataSetBase<M> }

constructor TDataSetBase<M>.Create(AConnection: IDBConnection; ADataSet: TDataSet; APageSize: Integer; AMasterObject: TObject);
begin
  FConnection := AConnection;
  FOrmDataSet := ADataSet;
  FPageSize := APageSize;
  FSession := TSessionDataSet<M>.Create(AConnection, APageSize);
  FSession.UseDataSet := True;
  FOrmDataSetEvents := TDataSet.Create(nil);
  FMasterObject := TDictionary<string, TDataSetBase<M>>.Create;
  FModifiedFields := TDictionary<string, TField>.Create;
  FLookupsField := TList<TDataSetBase<M>>.Create;
  FCurrentRecordInternal := M.Create;
  FCurrentRecord := M.Create;
  FOrmDataSource := TDataSource.Create(nil);
  FOrmDataSource.DataSet := FOrmDataSet;
  FOrmDataSource.OnDataChange  := DoDataChange;
  FOrmDataSource.OnStateChange := DoStateChange;
  FOrmDataSource.OnUpdateData  := DoUpdateData;
  TBindDataSet.GetInstance.SetInternalInitFieldDefsObjectClass(ADataSet, FCurrentRecord);
  TBindDataSet.GetInstance.SetDataDictionary(ADataSet, FCurrentRecord);
  FDataSetEvents := TDataSetEvents.Create;
  FInternalIndex := 0;
  if AMasterObject <> nil then
     SetMasterObject(AMasterObject);
end;

destructor TDataSetBase<M>.Destroy;
var
  iLookup: Integer;
begin
  FOrmDataSet  := nil;
  FOwnerMasterObject := nil;
  FSession.Free;
  FOrmDataSource.Free;
  FDataSetEvents.Free;
  FOrmDataSetEvents.Free;
  FCurrentRecord.Free;
  FCurrentRecordInternal.Free;
  FModifiedFields.Clear;
  FModifiedFields.Free;
  FMasterObject.Clear;
  FMasterObject.Free;
  if Assigned(FLookupsField) then
     for iLookup := 0 to FLookupsField.Count -1 do
        FLookupsField.Items[iLookup].Close;
  FLookupsField.Clear;
  FLookupsField.Free;
  inherited;
end;

procedure TDataSetBase<M>.CancelUpdates;
begin
  FModifiedFields.Clear;
end;

procedure TDataSetBase<M>.Close;
begin
  FOrmDataSet.Close;
end;

procedure TDataSetBase<M>.AddAggregateField(AFieldName: string;
                                            AExpression: string;
                                            AAlignment: TAlignment;
                                            ADisplayFormat: string);
begin
  TFieldSingleton.GetInstance.AddAggregateField(FOrmDataSet,
                                                AFieldName,
                                                AExpression,
                                                AAlignment,
                                                ADisplayFormat);
end;

procedure TDataSetBase<M>.AddCalcField(AFieldName: String;
                                       AFieldType: TFieldType;
                                       ASize: Integer;
                                       ADisplayFormat: string);
begin
  TFieldSingleton.GetInstance.AddCalcField(FOrmDataSet,
                                           AFieldName,
                                           AfieldType,
                                           ASize,
                                           ADisplayFormat);

end;

procedure TDataSetBase<M>.AddLookupField(AFieldName: string;
                                         AKeyFields: string;
                                         ALookupDataSet: TObject;
                                         ALookupKeyFields: string;
                                         ALookupResultField: string);
var
  oColumn: TColumnMapping;
  oColumns: TColumnMappingList;
begin
  /// Guarda o datasetlookup em uma lista para controle interno
  FLookupsField.Add(TDataSetBase<M>(ALookupDataSet));
  oColumns := FSession.Explorer.GetMappingColumn(FLookupsField.Last.FCurrentRecord.ClassType);
  if oColumns <> nil then
  begin
    for oColumn in oColumns do
    begin
       if oColumn.Name = ALookupResultField then
       begin
          TFieldSingleton.GetInstance.AddLookupField(AFieldName,
                                                     FOrmDataSet,
                                                     AKeyFields,
                                                     FLookupsField.Last.FOrmDataSet,
                                                     ALookupKeyFields,
                                                     ALookupResultField,
                                                     oColumn.FieldType,
                                                     oColumn.Size);
          /// Abre a tabela lookup
          FLookupsField.Last.Open;
       end;
    end;
  end;
end;

procedure TDataSetBase<M>.Append;
begin
  FOrmDataSet.Insert;
  TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, TObject(FCurrentRecord));
end;

procedure TDataSetBase<M>.EnableDataSetEvents;
var
  oClassType: TRttiType;
  oProperty: TRttiProperty;
  oPropInfo: PPropInfo;
  oMethod: TMethod;
  oMethodNil: TMethod;
begin
  oClassType := TRttiSingleton.GetInstance.GetRttiType(FOrmDataSet.ClassType);
  for oProperty in oClassType.GetProperties do
  begin
    if FindEvents(oProperty.Name) then
    begin
      oPropInfo := GetPropInfo(FOrmDataSet, oProperty.Name);
      if (oPropInfo <> nil) and (oPropInfo^.PropType^^.Kind = tkMethod) then
      begin
         oMethod := GetMethodProp(FOrmDataSetEvents, oPropInfo);
         if Assigned(oMethod.Code) then
         begin
            oMethodNil.Code := nil;
            SetMethodProp(FOrmDataSet, oPropInfo, oMethod);
            SetMethodProp(FOrmDataSetEvents, oPropInfo, oMethodNil);
         end;
      end;
    end;
  end;
end;

procedure TDataSetBase<M>.FillDataSet;
var
  oEnumerator: TEnumerator<M>;
begin
   if not FOrmDataSet.Active then
      raise Exception.Create('DataSet n�o est� ativo!');

   oEnumerator := FSession.Manager.List.GetEnumerator;
   try
     while oEnumerator.MoveNext do
     begin
       FOrmDataSet.Append;
       TBindDataSet.GetInstance.SetPropertyToField(TObject(FCurrentRecord), FOrmDataSet);
       FOrmDataSet.Post;
     end;
   finally
     oEnumerator.Free;
   end;
end;

procedure TDataSetBase<M>.FillList;
var
  oBookMark: TBookmark;
  oChild: TPair<string, TDataSetBase<M>>;
begin
   FOrmDataSet.DisableControls;
   DisableDataSetEvents;
   for oChild in FMasterObject do
     oChild.Value.FOrmDataSet.DisableControls;
   { Guarda a posi��o atual do registro }
   oBookMark := FOrmDataSet.Bookmark;
   try
     { Limpa a lista de objetos }
     FSession.Manager.List.Clear;
     FOrmDataSet.First;
     while not FOrmDataSet.Eof do
     begin
        { Cria e adiciona o Objeto M na lista de objetos }
        FSession.Manager.Add;
        { Popula o objeto M e o adiciona na lista e objetos com o registro do DataSet }
        TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, TObject(FSession.Manager.List.Last));

        for oChild in FMasterObject do
          FillMastersClass(oChild.Value, FSession.Manager.List.Last);
        { Pr�ximo registro }
        FOrmDataSet.Next;
     end;
   finally
     { Posiciona o curso no registro que estava no inicio }
     FOrmDataSet.GotoBookmark(oBookMark);
     FOrmDataSet.FreeBookmark(oBookMark);
     FOrmDataSet.EnableControls;
     for oChild in FMasterObject do
       oChild.Value.FOrmDataSet.EnableControls;
   end;
end;

procedure TDataSetBase<M>.FillMastersClass(ADatasetBase: TDataSetBase<M>; AObject: M);
var
  oRttiType: TRttiType;
  oProperty: TRttiProperty;
  oAttrProperty: TCustomAttribute;
begin
  oRttiType := TRttiSingleton.GetInstance.GetRttiType(AObject.ClassType);
  for oProperty in oRttiType.GetProperties do
  begin
     for oAttrProperty in oProperty.GetAttributes do
     begin
        if oAttrProperty is Association then // Association
        begin
           if Association(oAttrProperty).Multiplicity = OneToOne then /// OneToOne
              ExecuteOneToOne(AObject, oProperty, ADatasetBase)
           else
           if Association(oAttrProperty).Multiplicity = OneToMany then /// OneToMany
              ExecuteOneToMany(AObject, oProperty, ADatasetBase, oRttiType);
        end;
     end;
  end;
end;

procedure TDataSetBase<M>.ExecuteOneToOne(AObject: M; oProperty: TRttiProperty;
  ADatasetBase: TDataSetBase<M>);
var
  oBookMark: TBookmark;
begin
  if ADatasetBase.FCurrentRecord.ClassType = oProperty.PropertyType.AsInstance.MetaclassType then
  begin
    oBookMark := ADatasetBase.FOrmDataSet.Bookmark;
    while not ADatasetBase.FOrmDataSet.Eof do
    begin
      /// Popula o objeto M e o adiciona na lista e objetos com o registro do DataSet.
      TBindDataSet.GetInstance.SetFieldToProperty(ADatasetBase.FOrmDataSet, oProperty.GetValue(TObject(AObject)).AsObject);
      /// Pr�ximo registro
      ADatasetBase.FOrmDataSet.Next;
    end;
    ADatasetBase.FOrmDataSet.GotoBookmark(oBookMark);
    ADatasetBase.FOrmDataSet.FreeBookmark(oBookMark);
  end;
end;

procedure TDataSetBase<M>.ExecuteOneToMany(AObject: M; oProperty: TRttiProperty;
  ADatasetBase: TDataSetBase<M>; oRttiType: TRttiType);
var
  oPropertyType: TRttiType;
  oBookMark: TBookmark;
  oObjectType: TObject;
  oObjectList: TObject;
begin
  oPropertyType := oRttiType.GetProperty(oProperty.Name).PropertyType;
  oPropertyType := TRttiSingleton.GetInstance.GetListType(oPropertyType);
  if not oPropertyType.IsInstance then
    raise Exception.Create('Not in instance ' + oPropertyType.Parent.ClassName + ' - ' + oPropertyType.Name);
  ///
  if ADatasetBase.FCurrentRecord.ClassType = oPropertyType.AsInstance.MetaclassType then
  begin
    oBookMark := ADatasetBase.FOrmDataSet.Bookmark;
    while not ADatasetBase.FOrmDataSet.Eof do
    begin
      oObjectType := oPropertyType.AsInstance.MetaclassType.Create;
      /// Popula o objeto M e o adiciona na lista e objetos com o registro do DataSet.
      TBindDataSet.GetInstance.SetFieldToProperty(ADatasetBase.FOrmDataSet, oObjectType);
      ///
      oObjectList := oProperty.GetValue(TObject(AObject)).AsObject;
      TRttiSingleton.GetInstance.MethodCall(oObjectList, 'Add', [oObjectType]);
      /// Pr�ximo registro
      ADatasetBase.FOrmDataSet.Next;
    end;
    ADatasetBase.FOrmDataSet.GotoBookmark(oBookMark);
    ADatasetBase.FOrmDataSet.FreeBookmark(oBookMark);
  end;
end;

procedure TDataSetBase<M>.DisableDataSetEvents;
var
  oClassType: TRttiType;
  oProperty: TRttiProperty;
  oPropInfo: PPropInfo;
  oMethod: TMethod;
  oMethodNil: TMethod;
begin
  oClassType := TRttiSingleton.GetInstance.GetRttiType(FOrmDataSet.ClassType);
  for oProperty in oClassType.GetProperties do
  begin
    if FindEvents(oProperty.Name) then
    begin
      oPropInfo := GetPropInfo(FOrmDataSet, oProperty.Name);
      if (oPropInfo <> nil) and (oPropInfo^.PropType^^.Kind = tkMethod) then
      begin
         oMethod := GetMethodProp(FOrmDataSet, oPropInfo);
         if Assigned(oMethod.Code) then
         begin
            oMethodNil.Code := nil;
            SetMethodProp(FOrmDataSetEvents, oPropInfo, oMethod);
            SetMethodProp(FOrmDataSet, oPropInfo, oMethodNil);
         end;
      end;
    end;
  end;
end;

function TDataSetBase<M>.FindEvents(AEventName: string): Boolean;
begin
  Result := MatchStr(AEventName, ['AfterCancel'   ,'AfterClose'   ,'AfterDelete' ,
                                  'AfterEdit'     ,'AfterInsert'  ,'AfterOpen'   ,
                                  'AfterPost'     ,'AfterRefresh' ,'AfterScroll' ,
                                  'BeforeCancel'  ,'BeforeClose'  ,'BeforeDelete',
                                  'BeforeEdit'    ,'BeforeInsert' ,'BeforeOpen'  ,
                                  'BeforePost'    ,'BeforeRefresh','BeforeScroll',
                                  'OnCalcFields'  ,'OnDeleteError','OnEditError' ,
                                  'OnFilterRecord','OnNewRecord'  ,'OnPostError']);
end;

procedure TDataSetBase<M>.DoAfterClose(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterClose) then
    FDataSetEvents.AfterClose(DataSet);
end;

procedure TDataSetBase<M>.DoAfterDelete(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterDelete) then
    FDataSetEvents.AfterDelete(DataSet);
end;

procedure TDataSetBase<M>.DoAfterPost(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterPost) then
    FDataSetEvents.AfterPost(DataSet);
end;

procedure TDataSetBase<M>.DoAfterScroll(DataSet: TDataSet);
begin
  if DataSet.State in [dsBrowse] then
     OpenDataSetChilds;
  if Assigned(FDataSetEvents.AfterScroll) then
    FDataSetEvents.AfterScroll(DataSet);
  /// <summary>
  /// Controle de pagina��o de registros retornados do banco de dados
  /// </summary>
  if FPageSize > -1 then
    if FOrmDataSet.Eof then
      GetNextPacket;
end;

function TDataSetBase<M>.GetOneToOneRelationFields(ATable: TTableMapping; ADetail: TDataSetBase<M>; var ASQLBuild: ICriteria): Boolean;
var
  oAssociation: TAssociationMappingList;
  iFor,iCol: Integer;
begin
   Result := False;
   oAssociation := FSession.Explorer.GetMappingAssociation(FCurrentRecord.ClassType);
   if oAssociation <> nil then
   begin
      /// Get Lookup
      for iFor := 0 to oAssociation.Count -1 do
      begin
         /// <summary>
         /// Verifica��o se tem algum mapeamento OneToOne para a classe.
         /// </summary>
         if oAssociation.Items[iFor].ReferencedClassName = ADetail.FCurrentRecord.ClassName then
         begin
            /// <summary>
            /// Verifica��o se foi relacionado uma lista de campos para Select, caso negativo Select All
            /// </summary>
            if oAssociation.Items[iFor].Count > 0 then
            begin
              ASQLBuild.Column(oAssociation.Items[iFor].ReferencedColumnName);
              for iCol := 0 to oAssociation.Items[iFor].Count -1 do
                ASQLBuild.Column(oAssociation.Items[iFor].Columns[iCol]);
            end
            else
              ASQLBuild.All;
            /// <summary>
            /// From pelo nome da classe de referencia e aplicado o Where pela Coluna de referencia.
            /// </summary>
            ASQLBuild.From(ATable.Name);
            ASQLBuild.Where(oAssociation.Items[iFor].ReferencedColumnName + '=' + TBindDataSet.GetInstance.GetFieldValue(FOrmDataSet,
                                                                                                                         oAssociation.Items[iFor].ColumnName,
                                                                                                                         FOrmDataSet.FieldByName(oAssociation.Items[iFor].ColumnName).DataType));
            Result := True;
         end;
      end;
   end;
end;

procedure TDataSetBase<M>.Lazy(AOwner: TObject);
var
  oTable: TTableMapping;
  oCriteria: ICriteria;
begin
  if AOwner <> nil then
  begin
    if FOwnerMasterObject = nil then
    begin
      if not FOrmDataSet.Active then
      begin
        SetMasterObject(AOwner);
        oTable := FSession.Explorer.GetMappingTable(FCurrentRecord.ClassType);
        if oTable <> nil then
        begin
          oCriteria := CreateCriteria.Select;
          if not TDataSetBase<M>(FOwnerMasterObject).GetOneToOneRelationFields(oTable, Self, oCriteria) then
             TDataSetBase<M>(FOwnerMasterObject).GetOneToManyRelationFields(Self, oCriteria);
          Open(oCriteria);
        end;
      end;
    end;
  end
  else
  begin
    if FOwnerMasterObject <> nil then
    begin
      if TDataSetBase<M>(FOwnerMasterObject).FOrmDataSet.Active then
      begin
        SetMasterObject(nil);
        Close;
      end;
    end
  end;
end;

function TDataSetBase<M>.GetOneToManyRelationFields(ADetail: TDataSetBase<M>; var ASQLBuild: ICriteria): Boolean;
var
  oAssociation: TAssociationMappingList;
  iFor: Integer;
begin
   Result := False;
   /// Get Ids Entity Master
   oAssociation := FSession.Explorer.GetMappingAssociation(ADetail.FCurrentRecord.ClassType);
   if oAssociation <> nil then
   begin
      for iFor := 0 to oAssociation.Count -1 do
      begin
         ASQLBuild.Where(oAssociation.Items[iFor].ReferencedColumnName + '=' + TBindDataSet.GetInstance.GetFieldValue(FOrmDataSet,
                                                                                                                           oAssociation.Items[iFor].ColumnName,
                                                                                                                           FOrmDataSet.FieldByName(oAssociation.Items[iFor].ColumnName).DataType));
         Result := True;
      end;
   end;
end;

procedure TDataSetBase<M>.DoBeforeCancel(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeCancel) then
     FDataSetEvents.BeforeCancel(DataSet);
end;

procedure TDataSetBase<M>.DoAfterCancel(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.AfterCancel) then
     FDataSetEvents.AfterCancel(DataSet);
end;

procedure TDataSetBase<M>.DoBeforeClose(DataSet: TDataSet);
var
  oChild: TPair<string, TDataSetBase<M>>;
begin
  if Assigned(FDataSetEvents.BeforeClose) then
     FDataSetEvents.BeforeClose(DataSet);

  if Assigned(FMasterObject) then
    if FMasterObject.Count > 0 then
      for oChild in FMasterObject do
        oChild.Value.Close;
end;

procedure TDataSetBase<M>.DoBeforeDelete(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeDelete) then
     FDataSetEvents.BeforeDelete(DataSet);
  EmptyDataSetChilds;
end;

procedure TDataSetBase<M>.DoBeforePost(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforePost) then
     FDataSetEvents.BeforePost(DataSet);
end;

procedure TDataSetBase<M>.DoBeforeScroll(DataSet: TDataSet);
begin
  if Assigned(FDataSetEvents.BeforeScroll) then
    FDataSetEvents.BeforeScroll(DataSet);
end;

procedure TDataSetBase<M>.DoDataChange(Sender: TObject; Field: TField);
begin
  if FOrmDataSet.State in [dsEdit] then
  begin
    if Field <> nil then
       if Field.FieldKind = fkData then
         if Field.FieldName <> cInternalField then
           if not FModifiedFields.ContainsKey(Field.FieldName) then
             FModifiedFields.Add(Field.FieldName,Field);
  end;
end;

procedure TDataSetBase<M>.DoNewRecord(DataSet: TDataSet);
begin
  /// <summary>
  /// Limpa os datasets em mem�ria para receberem novos valores
  /// </summary>
  EmptyDataSetChilds;
  /// <summary>
  /// Busca valor da tabela master, caso aqui seja um tabela detalhe.
  /// </summary>
  GetMasterValues;
  if Assigned(FDataSetEvents.OnNewRecord) then
    FDataSetEvents.OnNewRecord(DataSet);
end;

procedure TDataSetBase<M>.DoStateChange(Sender: TObject);
begin
   if FOrmDataSet.State in [dsInsert] then
     FOrmDataSet.Fields[FInternalIndex].AsInteger := Integer(FOrmDataSet.State)
   else
   if FOrmDataSet.State in [dsEdit] then
     if FOrmDataSet.Fields[FInternalIndex].AsInteger = -1 then
       FOrmDataSet.Fields[FInternalIndex].AsInteger := Integer(FOrmDataSet.State);
end;

procedure TDataSetBase<M>.DoUpdateData(Sender: TObject);
begin

end;

procedure TDataSetBase<M>.Delete;
begin
  FOrmDataSet.Delete;
end;

procedure TDataSetBase<M>.Edit;
begin
  FOrmDataSet.Edit;
end;

procedure TDataSetBase<M>.GetDataSetEvents;
begin
  /// Scroll Events
  if Assigned(FOrmDataSet.BeforeScroll) then FDataSetEvents.BeforeScroll := FOrmDataSet.BeforeScroll;
  if Assigned(FOrmDataSet.AfterScroll)  then FDataSetEvents.AfterScroll  := FOrmDataSet.AfterScroll;
  /// Close Events
  if Assigned(FOrmDataSet.BeforeClose)  then FDataSetEvents.BeforeClose := FOrmDataSet.BeforeClose;
  if Assigned(FOrmDataSet.AfterClose)   then FDataSetEvents.AfterClose  := FOrmDataSet.AfterClose;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforeDelete) then FDataSetEvents.BeforeDelete := FOrmDataSet.BeforeDelete;
  if Assigned(FOrmDataSet.AfterDelete)  then FDataSetEvents.AfterDelete  := FOrmDataSet.AfterDelete;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforePost) then FDataSetEvents.BeforePost := FOrmDataSet.BeforePost;
  if Assigned(FOrmDataSet.AfterPost)  then FDataSetEvents.AfterPost  := FOrmDataSet.AfterPost;
  /// Delete Events
  if Assigned(FOrmDataSet.BeforeCancel) then FDataSetEvents.BeforeCancel := FOrmDataSet.BeforeCancel;
  if Assigned(FOrmDataSet.AfterCancel)  then FDataSetEvents.AfterCancel  := FOrmDataSet.AfterCancel;
  /// NewRecord Events
  if Assigned(FOrmDataSet.OnNewRecord)  then FDataSetEvents.OnNewRecord := FOrmDataSet.OnNewRecord;
end;

function TDataSetBase<M>.Find: Boolean;
var
  oChild: TPair<string, TDataSetBase<M>>;
begin
   Result := False;
   if FOrmDataSet.Active then
   begin
      if FOrmDataSet.RecordCount > 0 then
      begin
         TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, TObject(FCurrentRecord));
         for oChild in FMasterObject do
            oChild.Value.FillMastersClass(oChild.Value, FCurrentRecord);
         Result := True;
      end;
   end;
end;

function TDataSetBase<M>.GetCurrentRecordInternal: M;
begin
   if FOrmDataSet.Active then
      if FOrmDataSet.RecordCount > 0 then
         TBindDataSet.GetInstance.SetFieldToProperty(FOrmDataSet, TObject(FCurrentRecordInternal));
   Result := FCurrentRecordInternal;
end;

procedure TDataSetBase<M>.Open(ASQL: ICriteria; AID: TValue);
var
  oDBResultSet: IDBResultSet;
begin
  if AID.AsInteger > -1 then
    oDBResultSet := FSession.Open(AID)
  else
    oDBResultSet := FSession.Open(ASQL);
  /// Popula DataSet
  while oDBResultSet.NotEof do
  begin
     FOrmDataSet.Append;
     TBindDataSet.GetInstance.SetFieldToField(oDBResultSet, FOrmDataSet);
     FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
     FOrmDataSet.Post;
  end;
end;

function TDataSetBase<M>.GetNextPacket: Boolean;
var
  oDBResultSet: IDBResultSet;
  oBookMark: TBookmark;
begin
  Result := False;
  if not FSession.FetchingRecords then
  begin
     FOrmDataSet.DisableControls;
     DisableDataSetEvents;
     oBookMark := FOrmDataSet.Bookmark;
     try
       oDBResultSet := FSession.GetNextPacket;
       /// Popula DataSet
       while oDBResultSet.NotEof do
       begin
          FOrmDataSet.Append;
          TBindDataSet.GetInstance.SetFieldToField(oDBResultSet, FOrmDataSet);
          FOrmDataSet.Fields[FInternalIndex].AsInteger := -1;
          FOrmDataSet.Post;
       end;
       Result := True;
     finally
       FOrmDataSet.GotoBookmark(oBookMark);
       FOrmDataSet.EnableControls;
       EnableDataSetEvents;
     end;
  end;
end;

procedure TDataSetBase<M>.Open;
begin
  Open(CreateCriteria);
end;

procedure TDataSetBase<M>.OpenDataSetChilds;
var
  oTable: TTableMapping;
  oChild: TPair<string, TDataSetBase<M>>;
  oCriteria: ICriteria;
begin
  if FOrmDataSet.Active then
  begin
     if not FOrmDataSet.Eof then
     begin
        /// <summary>
        /// Se Count > 0 identifica-se que � o objeto � um Master
        /// </summary>
        if FMasterObject.Count > 0 then
        begin
           for oChild in FMasterObject do
           begin
              oTable := FSession.Explorer.GetMappingTable(oChild.Value.FCurrentRecord.ClassType);
              if oTable <> nil then
              begin
                oChild.Value.Close;
                oCriteria := CreateCriteria.Select;
                if not GetOneToOneRelationFields(oTable, oChild.Value, oCriteria) then
                   GetOneToManyRelationFields(oChild.Value, oCriteria);
                oChild.Value.Open(oCriteria);
              end;
           end;
        end;
     end;
  end;
end;

procedure TDataSetBase<M>.Open(AID: TValue);
begin
  Open(CreateCriteria, AID);
end;

procedure TDataSetBase<M>.Open(ASQL: ICriteria);
begin
  Open(ASQL, -1);
end;

procedure TDataSetBase<M>.Post;
begin
  TBindDataSet.GetInstance.SetPropertyToField(FCurrentRecord, FOrmDataSet);
  FOrmDataSet.Post;
end;

procedure TDataSetBase<M>.RefreshRecord;
var
  oDBResultSet: IDBResultSet;
  oPrimaryKey: TPrimaryKeyMapping;
begin
  oPrimaryKey := FSession.Explorer.GetMappingPrimaryKey(FCurrentRecord.ClassType);
  if oPrimaryKey <> nil then
  begin
    FOrmDataSet.DisableControls;
    DisableDataSetEvents;
    try
      oDBResultSet := FSession.Open(FOrmDataSet.FieldByName(oPrimaryKey.Columns[0]).AsInteger);
      /// Popula DataSet
      while oDBResultSet.NotEof do
      begin
         FOrmDataSet.Edit;
         TBindDataSet.GetInstance.SetFieldToField(oDBResultSet, FOrmDataSet);
         FOrmDataSet.Post;
      end;
    finally
      FOrmDataSet.EnableControls;
      EnableDataSetEvents;
    end;
  end;
end;

procedure TDataSetBase<M>.SetDataSetEvents;
begin
   FOrmDataSet.BeforeScroll := DoBeforeScroll;
   FOrmDataSet.AfterScroll  := DoAfterScroll;
   FOrmDataSet.BeforeClose  := DoBeforeClose;
   FOrmDataSet.AfterClose   := DoAfterClose;
   FOrmDataSet.BeforeDelete := DoBeforeDelete;
   FOrmDataSet.AfterDelete  := DoAfterDelete;
   FOrmDataSet.BeforePost   := DoBeforePost;
   FOrmDataSet.AfterPost    := DoAfterPost;
   FOrmDataSet.OnNewRecord  := DoNewRecord;
end;

procedure TDataSetBase<M>.GetMasterValues;
var
  oOneToMany: TAssociationMapping;
  oOneToManyList: TAssociationMappingList;
  oCurrentMaster: TDataSetBase<M>;
begin
  if Assigned(FOwnerMasterObject) then
  begin
    oCurrentMaster := TDataSetBase<M>(FOwnerMasterObject);
    oOneToManyList := FSession.Explorer.GetMappingAssociation(oCurrentMaster.FCurrentRecord.ClassType);
    if oOneToManyList <> nil then
    begin
      for oOneToMany in oOneToManyList do
        if oOneToMany.Multiplicity = OneToMany then // OneToMany
          FOrmDataSet.FieldByName(oOneToMany.ColumnName).Value := oCurrentMaster.FOrmDataSet.FieldByName(oOneToMany.ColumnName).Value;
    end;
  end;
end;

procedure TDataSetBase<M>.SetMasterObject(const AValue: TObject);
begin
  if FOwnerMasterObject <> AValue then
  begin
    if FOwnerMasterObject <> nil then
      if TDataSetBase<M>(FOwnerMasterObject).FMasterObject.ContainsKey(FCurrentRecord.ClassName) then
        TDataSetBase<M>(FOwnerMasterObject).FMasterObject.Remove(FCurrentRecord.ClassName);

    if AValue <> nil then
      TDataSetBase<M>(AValue).FMasterObject.Add(FCurrentRecord.ClassName, Self);

    FOwnerMasterObject := AValue;
  end;
end;

end.
