{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.bind.dataset;

interface

uses
  Classes,
  SysUtils,
  Rtti,
  DB,
  TypInfo,
  Variants,
  /// orm
  ormbr.mapping.attributes,
  ormbr.mapping.rttiutils,
  ormbr.mapping.exceptions,
  ormbr.factory.interfaces,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  ormbr.types.nullable;

type
  TBindDataSet = class
  private
  class var
    FInstance: TBindDataSet;
    FContext: TRttiContext;
  private
    constructor CreatePrivate;
  public
    { Public declarations }
    constructor Create;
    class function GetInstance: TBindDataSet;
    procedure SetDataDictionary(ADataSet: TDataSet; AObject: TObject);
    procedure SetInternalInitFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
    procedure SetPropertyToField(AObject: TObject; ADataSet: TDataSet);
    procedure SetFieldToProperty(ADataSet: TDataSet; AObject: TObject);
    procedure SetFieldToField(AResultSet: IDBResultSet; ADataSet: TDataSet);
    function GetFieldValue(ADataSet: TDataSet; AFieldName: string; AFieldType: TFieldType): string;
  end;

implementation

uses
  ormbr.bind.fields,
  ormbr.types.mapping;

{ TBindDataSet }

procedure TBindDataSet.SetPropertyToField(AObject: TObject; ADataSet: TDataSet);
var
  oProperty: TRttiProperty;
  iFieldIndex: Integer;
begin
   for oProperty in AObject.GetColumns do
   begin
     /// IsJoinColumn
     if oProperty.IsJoinColumn then
       Continue;
     ///
     iFieldIndex := (oProperty as TRttiInstanceProperty).Index + 1;
     if SameText(ADataSet.Fields[iFieldIndex].FieldName, oProperty.Name) then
     begin
       if TRttiSingleton.GetInstance.IsNullable(oProperty.PropertyType.Handle) then
         ADataSet.Fields[iFieldIndex].Value := oProperty.GetNullableValue(AObject).AsVariant
       else
         ADataSet.Fields[iFieldIndex].Value := oProperty.GetValue(AObject).AsVariant;
     end;
  end;
end;

constructor TBindDataSet.Create;
begin
   raise Exception.Create('Para usar o BindDataSet use o m�todo TBindDataSet.GetInstance()');
end;

constructor TBindDataSet.CreatePrivate;
begin
   inherited;
   FContext := TRttiContext.Create;
end;

function TBindDataSet.GetFieldValue(ADataSet: TDataSet; AFieldName: string; AFieldType: TFieldType): string;
begin
  case AFieldType of
    ftUnknown: ;
    ftString, ftDate, ftTime, ftDateTime, ftTimeStamp:
    begin
       Result := QuotedStr(ADataSet.FieldByName(AFieldName).AsString);
    end
    else
       Result := ADataSet.FieldByName(AFieldName).AsString;
{
   ftSmallint: ;
   ftInteger:
   ftWord: ;
   ftBoolean: ;
   ftFloat: ;
   ftCurrency: ;
   ftBCD: ;
   ftBytes: ;
   ftVarBytes: ;
   ftAutoInc: ;
   ftBlob: ;
   ftMemo: ;
   ftGraphic: ;
   ftFmtMemo: ;
   ftParadoxOle: ;
   ftDBaseOle: ;
   ftTypedBinary: ;
   ftCursor: ;
   ftFixedChar: ;
   ftWideString: ;
   ftLargeint: ;
   ftADT: ;
   ftArray: ;
   ftReference: ;
   ftDataSet: ;
   ftOraBlob: ;
   ftOraClob: ;
   ftVariant: ;
   ftInterface: ;
   ftIDispatch: ;
   ftGuid: ;
   ftFMTBcd: ;
   ftFixedWideChar: ;
   ftWideMemo: ;
   ftOraTimeStamp: ;
   ftOraInterval: ;
   ftLongWord: ;
   ftShortint: ;
   ftByte: ;
   ftExtended: ;
   ftConnection: ;
   ftParams: ;
   ftStream: ;
   ftTimeStampOffset: ;
   ftObject: ;
   ftSingle: ;
}
  end;
end;

class function TBindDataSet.GetInstance: TBindDataSet;
begin
   if not Assigned(FInstance) then
      FInstance := TBindDataSet.CreatePrivate;

   Result := FInstance;
end;

procedure TBindDataSet.SetDataDictionary(ADataSet: TDataSet; AObject: TObject);
var
  oProperty: TRttiProperty;
  oRttiAttr: TCustomAttribute;
  iFieldIndex: Integer;
begin
   for oProperty in AObject.GetDictionarys do
   begin
     iFieldIndex := (oProperty as TRttiInstanceProperty).Index + 1;
     if Assigned(TField(oProperty)) then
     begin
        oRttiAttr := AObject.GetDictionary(oProperty);
        /// DisplayLabel
        if Length(Dictionary(oRttiAttr).DisplayLabel) > 0 then
           ADataSet.Fields[iFieldIndex].DisplayLabel := Dictionary(oRttiAttr).DisplayLabel;

        /// ConstraintErrorMessage
        if Length(Dictionary(oRttiAttr).ConstraintErrorMessage) > 0 then
           ADataSet.Fields[iFieldIndex].ConstraintErrorMessage := Dictionary(oRttiAttr).ConstraintErrorMessage;

        /// DefaultExpression
        if Length(Dictionary(oRttiAttr).DefaultExpression) > 0 then
        begin
           if Dictionary(oRttiAttr).DefaultExpression = 'Date' then
              ADataSet.Fields[iFieldIndex].DefaultExpression := QuotedStr(DateToStr(Date))
           else
           if Dictionary(oRttiAttr).DefaultExpression = 'Now' then
              ADataSet.Fields[iFieldIndex].DefaultExpression := QuotedStr(DateTimeToStr(Now))
           else
              ADataSet.Fields[iFieldIndex].DefaultExpression := Dictionary(oRttiAttr).DefaultExpression;
        end;
        /// DisplayFormat
        if Length(Dictionary(oRttiAttr).DisplayFormat) > 0 then
           TDateField(ADataSet.Fields[iFieldIndex]).DisplayFormat := Dictionary(oRttiAttr).DisplayFormat;

        /// EditMask
        if Length(Dictionary(oRttiAttr).EditMask) > 0 then
           ADataSet.Fields[iFieldIndex].EditMask := Dictionary(oRttiAttr).EditMask;

        /// Alignment
        if Dictionary(oRttiAttr).Alignment in [taLeftJustify,taRightJustify,taCenter] then
           ADataSet.Fields[iFieldIndex].Alignment := Dictionary(oRttiAttr).Alignment;
     end;
   end;
end;

procedure TBindDataSet.SetFieldToProperty(ADataSet: TDataSet; AObject: TObject);
var
  oProperty: TRttiProperty;
//  oRestriction: TCustomAttribute;
  iFieldIndex: Integer;
begin
   for oProperty in AObject.GetColumns do
   begin
     iFieldIndex := (oProperty as TRttiInstanceProperty).Index + 1;
     /// Required the restriction
//     oRestriction := AObject.GetRestriction(oProperty);
//     if oRestriction <> nil then
//       if NotNull in Restrictions(oRestriction).Restrictions then
//         if ADataSet.Fields[iFieldIndex].Value = Null then
//           raise EFieldNotNull.Create(oProperty.Name);

     if oProperty.PropertyType.TypeKind in [tkString, tkUString] then
        oProperty.SetValue(AObject, ADataSet.Fields[iFieldIndex].AsString)
     else
     if oProperty.PropertyType.TypeKind in [tkFloat] then
     begin
       if oProperty.PropertyType.Handle = TypeInfo(TDateTime) then
          oProperty.SetValue(AObject, ADataSet.Fields[iFieldIndex].AsDateTime)
       else
       if oProperty.PropertyType.Handle = TypeInfo(TTime) then
          oProperty.SetValue(AObject, ADataSet.Fields[iFieldIndex].AsDateTime)
       else
          oProperty.SetValue(AObject, ADataSet.Fields[iFieldIndex].AsCurrency)
     end
     else
     if oProperty.PropertyType.TypeKind in [tkRecord] then
     begin
        if TRttiSingleton.GetInstance.IsNullable(oProperty.PropertyType.Handle) then
           oProperty.SetNullableValue(AObject, oProperty.PropertyType.Handle, ADataSet.Fields[iFieldIndex].AsVariant);
     end
     else
     if oProperty.PropertyType.TypeKind in [tkInteger] then
        oProperty.SetValue(AObject, ADataSet.Fields[iFieldIndex].AsInteger)
   end;
end;

procedure TBindDataSet.SetFieldToField(AResultSet: IDBResultSet; ADataSet: TDataSet);
var
  iFor: Integer;
begin
  for iFor := 1 to ADataSet.Fields.Count -1 do
  begin
     if (ADataSet.Fields[iFor].FieldKind = fkData) and
        (ADataSet.Fields[iFor].FieldName <> cInternalField) then
     begin
        if ADataSet.Fields[iFor].ReadOnly then
        begin
          ADataSet.Fields[iFor].ReadOnly := False;
          try
            ADataSet.Fields[iFor].AsVariant := AResultSet.GetFieldValue(iFor -1);
          finally
            ADataSet.Fields[iFor].ReadOnly := True;
          end;
        end
        else
          ADataSet.Fields[iFor].AsVariant := AResultSet.GetFieldValue(iFor -1);
     end;
  end;
end;

procedure TBindDataSet.SetInternalInitFieldDefsObjectClass(ADataSet: TDataSet; AObject: TObject);
var
  oProperty: TRttiProperty;
  oRttiAttr: TCustomAttribute;
  oRestriction: TCustomAttribute;
  oPrimaryKey: PrimaryKey;
begin
  for oProperty in AObject.GetColumns do
  begin
    oRttiAttr := AObject.GetColumn(oProperty);
    TFieldSingleton.GetInstance.AddField(ADataSet,
                                         Column(oRttiAttr).ColumnName,
                                         Column(oRttiAttr).FieldType,
                                         Column(oRttiAttr).Size);
    /// IsJoinColumn
    if oProperty.IsJoinColumn then
      ADataSet.Fields[ADataSet.Fields.Count -1].ReadOnly := True;
    /// Required the restriction
    oRestriction := AObject.GetRestriction(oProperty);
    if oRestriction <> nil then
      if NotNull in Restrictions(oRestriction).Restrictions then
         ADataSet.Fields[ADataSet.Fields.Count -1].Required := True;
    oPrimaryKey := AObject.GetPrimaryKey(oProperty);
    if oPrimaryKey <> nil then
      if oPrimaryKey.SequenceType = AutoInc then
        ADataSet.FieldByName(oPrimaryKey.Columns[0]).DefaultExpression := '-1';
  end;
  /// TField para controle interno ao Dataset
  TFieldSingleton.GetInstance.AddField(ADataSet, cInternalField, ftInteger);
  ADataSet.Fields[ADataSet.Fields.Count -1].DefaultExpression := '-1';
  ADataSet.Fields[ADataSet.Fields.Count -1].Visible := False;
  ADataSet.Fields[ADataSet.Fields.Count -1].Index   := 0;
end;

initialization
finalization
   if Assigned(TBindDataSet.FInstance) then
   begin
      TBindDataSet.FInstance.Free;
      TBindDataSet.FContext.Free;
   end;
end.

