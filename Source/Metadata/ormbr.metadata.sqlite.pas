{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.sqlite;

interface

uses
  SysUtils,
  Variants,
  DB,
  ormbr.metadata.register,
  ormbr.metadata.extract,
  ormbr.database.mapping,
  ormbr.factory.interfaces,
  ormbr.mapping.rttiutils,
  ormbr.types.fields,
  ormbr.types.database;

type
  TCatalogMetadataSQLite = class(TCatalogMetadataAbstract)
  private
    procedure ResolveFieldType(AColumn: TColumnMIK; ATypeName: string);
  protected
    function GetSelectTables: string; override;
    function GetSelectTableColumns(ATableName: string): string; override;
    function GetSelectPrimaryKey(ATableName: string): string; override;
    function GetSelectForeignKey(ATableName: string): string; override;
    function GetSelectIndexe(ATableName: string): string; override;
    function GetSelectIndexeColumns(AIndexeName: string): string; override;
    function GetSelectTriggers(ATableName: string): string; override;
    function GetSelectViews: string;
    function GetSelectSequences: string; override;
    function Execute: IDBResultSet;
  public
    procedure GetCatalogs; override;
    procedure GetSchemas; override;
    procedure GetTables; override;
    procedure GetColumns(ATable: TTableMIK); override;
    procedure GetPrimaryKey(ATable: TTableMIK); override;
    procedure GetIndexeKeys(ATable: TTableMIK); override;
    procedure GetForeignKeys(ATable: TTableMIK); override;
    procedure GetTriggers(ATable: TTableMIK); override;
    procedure GetChecks(ATable: TTableMIK); override;
    procedure GetSequences; override;
    procedure GetProcedures; override;
    procedure GetFunctions; override;
    procedure GetViews; override;
    procedure GetDatabaseMetadata; override;
  end;

implementation

{ TSchemaExtractSQLite }

function TCatalogMetadataSQLite.Execute: IDBResultSet;
var
  oSQLQuery: IDBQuery;
begin
  inherited;
  oSQLQuery := FConnection.CreateQuery;
  try
    oSQLQuery.CommandText := FSQLText;
    Exit(oSQLQuery.ExecuteQuery);
  except
    raise
  end;
end;

procedure TCatalogMetadataSQLite.GetDatabaseMetadata;
begin
  inherited;
  GetCatalogs;
end;

procedure TCatalogMetadataSQLite.GetCatalogs;
begin
  inherited;
  FCatalogMetadata.Name := '';
  GetSchemas;
end;

procedure TCatalogMetadataSQLite.GetChecks(ATable: TTableMIK);
begin
  /// Not Suported.
end;

procedure TCatalogMetadataSQLite.GetSchemas;
begin
  inherited;
  FCatalogMetadata.Schema := '';
  GetTables;
end;

procedure TCatalogMetadataSQLite.GetTables;
var
  oDBResultSet: IDBResultSet;
  oTable: TTableMIK;
begin
  inherited;
  FSQLText := GetSelectTables;
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oTable := TTableMIK.Create(FCatalogMetadata);
    oTable.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oTable.Description := '';
    /// <summary>
    /// Extrair colunas da tabela
    /// </summary>
    GetColumns(oTable);
    /// <summary>
    /// Extrair Primary Key da tabela
    /// </summary>
    GetPrimaryKey(oTable);
    /// <summary>
    /// Extrair Foreign Keys da tabela
    /// </summary>
    GetForeignKeys(oTable);
    /// <summary>
    /// Extrair Indexes da tabela
    /// </summary>
    GetIndexeKeys(oTable);
    /// <summary>
    /// Extrair Checks da tabela
    /// </summary>
    GetChecks(oTable);
    /// <summary>
    /// Adiciona na lista de tabelas extraidas
    /// </summary>
    FCatalogMetadata.Tables.Add(UpperCase(oTable.Name), oTable);
  end;
end;

procedure TCatalogMetadataSQLite.GetColumns(ATable: TTableMIK);
var
  oDBResultSet: IDBResultSet;
  oColumn: TColumnMIK;
begin
  inherited;
  FSQLText := GetSelectTableColumns(ATable.Name);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oColumn := TColumnMIK.Create(ATable);
    oColumn.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oColumn.Description := oColumn.Name;
    oColumn.Position := VarAsType(oDBResultSet.GetFieldValue('cid'), varInteger);
    /// <summary>
    /// O m�todo ResolveTypeField() extrai e popula as propriedades relacionadas abaixo
    /// </summary>
    /// <param name="AColumn: TColumnMIK">Informar [oColumn] para receber informa��es adicionais
    /// </param>
    /// <param name="ATypeName: String">Campo com descri�ao do tipo que veio na extra��o do metadata
    /// </param>
    /// <remarks>
    /// Rela��o das propriedades que ser�o alimentadas no m�todo ResolveTypeField()
    /// oColumn.FieldType: TTypeField;
    /// oColumn.TypeName: string;
    /// oColumn.Size: Integer;
    /// oColumn.Precision: Integer;
    /// oColumn.Scale: Integer;
    /// </remarks>
    ResolveFieldType(oColumn, VarToStr(oDBResultSet.GetFieldValue('type')));
    ///
    oColumn.NotNull := oDBResultSet.GetFieldValue('notnull') = 1;
    oColumn.DefaultValue := VarToStr(oDBResultSet.GetFieldValue('dflt_value'));
    ATable.Fields.Add(FormatFloat('000000', oColumn.Position), oColumn);
  end;
end;

procedure TCatalogMetadataSQLite.GetPrimaryKey(ATable: TTableMIK);
var
  oDBResultSet: IDBResultSet;

  procedure GetPrimaryKeyColumns(APrimaryKey: TPrimaryKeyMIK);
  var
    oDBResultSet: IDBResultSet;
    oColumn: TColumnMIK;
  begin
    FSQLText := Format('PRAGMA table_info("%s")', [ATable.Name]);
    oDBResultSet := Execute;
    while oDBResultSet.NotEof do
    begin
      oColumn := TColumnMIK.Create(ATable);
      oColumn.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
      oColumn.NotNull := oDBResultSet.GetFieldValue('notnull') = 1;
      oColumn.Position := VarAsType(oDBResultSet.GetFieldValue('cid'), varInteger);
      APrimaryKey.Fields.Add(FormatFloat('000000', oColumn.Position), oColumn);
    end;
  end;

begin
  inherited;
  FSQLText := GetSelectPrimaryKey(ATable.Name);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    if VarAsType(oDBResultSet.GetFieldValue('pk'), varInteger) = 1 then
    begin
      ATable.PrimaryKey.Name := Format('PK_%s', [ATable.Name]);
      ATable.PrimaryKey.Description := '';
      /// <summary>
      /// Estrai as columnas da primary key
      /// </summary>
      GetPrimaryKeyColumns(ATable.PrimaryKey);
      Break
    end;
  end;
end;

procedure TCatalogMetadataSQLite.GetForeignKeys(ATable: TTableMIK);
var
  oDBResultSet: IDBResultSet;
  oForeignKey: TForeignKeyMIK;
  oFromField: TColumnMIK;
  oToField: TColumnMIK;
  iID: Integer;
begin
  inherited;
  iID := -1;
  FSQLText := GetSelectForeignKey(ATable.Name);
  /// <summary>
  /// No FireDAC ao executar o comando de extra��o das FKs de um table, e essa
  /// table n�o tiver FKs, ocorre um erro de Not ResultSet, mas se tiver tudo
  /// ocorre normalmente, por isso o tratamento com try except
  /// </summary>
  try
    oDBResultSet := Execute;
    while oDBResultSet.NotEof do
    begin
      if iID <> VarAsType(oDBResultSet.GetFieldValue('id'), varInteger) then
      begin
        oForeignKey := TForeignKeyMIK.Create(ATable);
        oForeignKey.Name := Format('FK_%s_%s', [VarToStr(oDBResultSet.GetFieldValue('table')), VarToStr(oDBResultSet.GetFieldValue('from'))]);
        oForeignKey.FromTable := VarToStr(oDBResultSet.GetFieldValue('table'));
        oForeignKey.OnUpdate := TRttiSingleton.GetInstance.GetRuleAction(VarToStr(oDBResultSet.GetFieldValue('on_update')));
        oForeignKey.OnDelete := TRttiSingleton.GetInstance.GetRuleAction(VarToStr(oDBResultSet.GetFieldValue('on_delete')));
        iID := VarAsType(oDBResultSet.GetFieldValue('id'), varInteger);
        ATable.ForeignKeys.Add(oForeignKey.Name, oForeignKey);
      end;
      /// <summary>
      /// Coluna tabela master
      /// </summary>
      oFromField := TColumnMIK.Create(ATable);
      oFromField.Name := VarToStr(oDBResultSet.GetFieldValue('from'));
      oForeignKey.FromFields.Add(oFromField.Name, oFromField);
      /// <summary>
      /// Coluna tabela filha
      /// </summary>
      oToField := TColumnMIK.Create(ATable);
      oToField.Name := VarToStr(oDBResultSet.GetFieldValue('to'));
      oForeignKey.ToFields.Add(oToField.Name, oToField);
    end;
  except
  end;
end;

procedure TCatalogMetadataSQLite.GetFunctions;
begin
  inherited;

end;

procedure TCatalogMetadataSQLite.GetProcedures;
begin
  inherited;

end;

function TCatalogMetadataSQLite.GetSelectForeignKey(ATableName: string): string;
begin
  Result := Format('PRAGMA foreign_key_list("%s")', [ATableName]);
end;

function TCatalogMetadataSQLite.GetSelectIndexe(ATableName: string): string;
begin
 Result := Format('PRAGMA index_list("%s")', [ATableName]);
end;

function TCatalogMetadataSQLite.GetSelectIndexeColumns(AIndexeName: string): string;
begin
  Result := Format('PRAGMA index_info("%s")', [AIndexeName]);
end;

function TCatalogMetadataSQLite.GetSelectPrimaryKey(ATableName: string): string;
begin
  Result := Format('PRAGMA table_info("%s")', [ATableName]);
end;

function TCatalogMetadataSQLite.GetSelectSequences: string;
begin
  Result := ' select name ' +
            ' from sqlite_sequence ' +
            ' order by name';
end;

function TCatalogMetadataSQLite.GetSelectTableColumns(ATableName: string): string;
begin
  Result := Format('PRAGMA table_info("%s")', [ATableName]);
end;

function TCatalogMetadataSQLite.GetSelectTables: string;
begin
  Result := ' select name ' +
            ' from sqlite_master ' +
            ' where type = ''table'' ' +
            ' and tbl_name not like ''sqlite_%'' ' +
            ' order by name ';
end;

function TCatalogMetadataSQLite.GetSelectTriggers(ATableName: string): string;
begin
  Result := ' select name ' +
            ' from sqlite_master ' +
            ' where type = ''trigger'' ' +
            ' and tbl_name = ''' + ATableName + ''' ' +
            ' order by name ';

end;

function TCatalogMetadataSQLite.GetSelectViews: string;
begin
  Result := ' select name ' +
            ' from sqlite_master ' +
            ' where type = ''view'' ' +
            ' and tbl_name not like ''sqlite_%'' ' +
            ' order by name ';
end;

procedure TCatalogMetadataSQLite.GetSequences;
var
  oDBResultSet: IDBResultSet;
  oSequence: TSequenceMIK;
begin
  inherited;
  FSQLText := GetSelectSequences;
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oSequence := TSequenceMIK.Create(FCatalogMetadata);
    oSequence.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oSequence.Description := VarToStr(oDBResultSet.GetFieldValue('description'));;
    FCatalogMetadata.Sequences.Add(UpperCase(oSequence.Name), oSequence);
  end;
end;

procedure TCatalogMetadataSQLite.GetTriggers(ATable: TTableMIK);
var
  oDBResultSet: IDBResultSet;
  oTrigger: TTriggerMIK;
begin
  inherited;
  FSQLText := GetSelectTriggers(ATable.Name);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oTrigger := TTriggerMIK.Create(ATable);
    oTrigger.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oTrigger.Description := '';
    oTrigger.Script := VarToStr(oDBResultSet.GetFieldValue('sql'));
    ATable.Triggers.Add(UpperCase(oTrigger.Name), oTrigger);
  end;
end;

procedure TCatalogMetadataSQLite.GetIndexeKeys(ATable: TTableMIK);
var
  oDBResultSet: IDBResultSet;
  oIndexeKey: TIndexeKeyMIK;

  procedure GetIndexeKeyColumns(AIndexeKey: TIndexeKeyMIK);
  var
    oDBResultSet: IDBResultSet;
    oColumn: TColumnMIK;
  begin
    FSQLText := GetSelectIndexeColumns(AIndexeKey.Name);
    oDBResultSet := Execute;
    while oDBResultSet.NotEof do
    begin
      oColumn := TColumnMIK.Create(ATable);
      oColumn.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
      AIndexeKey.Fields.Add(oColumn.Name, oColumn);
    end;
  end;

begin
  inherited;
  FSQLText := GetSelectIndexe(ATable.Name);
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    if VarAsType(oDBResultSet.GetFieldValue('origin'), varString) = 'pk' then
       Continue;
    oIndexeKey := TIndexeKeyMIK.Create(ATable);
    oIndexeKey.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oIndexeKey.Unique := VarAsType(oDBResultSet.GetFieldValue('unique'), varInteger) = 1;
    ATable.IndexeKeys.Add(UpperCase(oIndexeKey.Name), oIndexeKey);
    /// <summary>
    /// Gera a lista de campos do indexe
    /// </summary>
    GetIndexeKeyColumns(oIndexeKey);
  end;
end;

procedure TCatalogMetadataSQLite.GetViews;
var
  oDBResultSet: IDBResultSet;
  oView: TViewMIK;
begin
  inherited;
  FSQLText := GetSelectViews;
  oDBResultSet := Execute;
  while oDBResultSet.NotEof do
  begin
    oView := TViewMIK.Create;
    oView.Name := VarToStr(oDBResultSet.GetFieldValue('name'));
    oView.Description := '';
    oView.Script := VarToStr(oDBResultSet.GetFieldValue('sql'));
    FCatalogMetadata.Views.Add(UpperCase(oView.Name), oView);
  end;
end;

procedure TCatalogMetadataSQLite.ResolveFieldType(AColumn: TColumnMIK; ATypeName: string);
var
  iPos1, iPos2: Integer;
  sDefArgs: string;

  procedure SetPrecScale(ADefPrec, ADefScale: Integer);
  var
    sSize, sPrecision, sCale: string;
    iPos: Integer;
  begin
    iPos := Pos(',', sDefArgs);
    if iPos = 0 then
      AColumn.Size := StrToIntDef(sDefArgs, ADefPrec)
    else
    begin
      sPrecision := Copy(sDefArgs, 1, iPos - 1);
      sCale := Copy(sDefArgs, iPos + 1, Length(sDefArgs));
      AColumn.Precision := StrToIntDef(sPrecision, ADefScale);
      AColumn.Scale := StrToIntDef(sCale, ADefPrec);
    end;
  end;

begin
  AColumn.FieldType := ftUnknown;
  AColumn.Size := 0;
  AColumn.Precision := 0;
  ATypeName := Trim(UpperCase(ATypeName));

  sDefArgs := '';

  iPos1 := Pos('(', ATypeName);
  iPos2 := Pos(')', ATypeName);
  if iPos1 > 0  then
  begin
    sDefArgs := Copy(ATypeName, iPos1 + 1, iPos2 - iPos1 - 1);
    ATypeName := Copy(ATypeName, 1, iPos1 - 1);
    SetPrecScale(0, 0);
  end;
  AColumn.FieldType := TFieldTypeRegister.GetInstance.GetFieldType(ATypeName);
  /// <summary>
  /// Resolve Field Type
  /// </summary>
  TFieldTypeRegister.GetInstance.GetFieldTypeDefinition(AColumn);
end;

initialization
  TMetadataRegister.GetInstance.RegisterMetadata(dnSQLite, TCatalogMetadataSQLite.Create);

end.
