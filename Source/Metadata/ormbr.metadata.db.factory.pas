{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.db.factory;

interface

uses
  SysUtils,
  Rtti,
  Generics.Collections,
  ormbr.metadata.register,
  ormbr.factory.interfaces,
  ormbr.metadata.extract,
  ormbr.database.mapping;

type
  TMetadataDBAbstract = class abstract
  protected
    FConnection: IDBConnection;
    FDatabaseMetadata: TCatalogMetadataAbstract;
    FCatalogMetadata: TCatalogMetadataMIK;
    procedure ExtractCatalogs; virtual; abstract;
    procedure ExtractSchemas; virtual; abstract;
    procedure ExtractTables; virtual; abstract;
  public
    constructor Create(AConnection: IDBConnection; ACatalogMetadata: TCatalogMetadataMIK); virtual;
    destructor Destroy; override;
    procedure ExtractMetadata; virtual; abstract;
  end;

  TMetadataDBFactory = class(TMetadataDBAbstract)
  public
    procedure ExtractMetadata; override;
  end;

implementation

{ TMetadataDBAbstract }

constructor TMetadataDBAbstract.Create(AConnection: IDBConnection; ACatalogMetadata: TCatalogMetadataMIK);
begin
  FConnection := AConnection;
  FCatalogMetadata := ACatalogMetadata;
  FDatabaseMetadata := TMetadataRegister.GetInstance.GetMetadata(AConnection.GetDriverName);
  FDatabaseMetadata.Connection := AConnection;
  FDatabaseMetadata.CatalogMetadata := FCatalogMetadata;
end;

destructor TMetadataDBAbstract.Destroy;
begin
  inherited;
end;

{ TMetadataFactory }

procedure TMetadataDBFactory.ExtractMetadata;
begin
  /// <summary>
  /// Extrair database metadata
  /// </summary>
  FDatabaseMetadata.GetDatabaseMetadata;
end;

end.
