{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(12 Out 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.ddl.generator.sqlite;

interface

uses
  SysUtils,
  StrUtils,
  Generics.Collections,
  ormbr.ddl.register,
  ormbr.ddl.generator,
  ormbr.types.database,
  ormbr.database.mapping;

type
  TDDLSQLGeneratorSQLite = class(TDDLSQLGenerator)
  protected
    function BuilderPrimayKeyDefinition(ATable: TTableMIK): string; override;
  public
    function GenerateCreateTable(ATable: TTableMIK): string; override;
    function GenerateCreateSequence(ASequence: TSequenceMIK): string; override;
    function GenerateDropTable(ATable: TTableMIK): string; override;
    function GenerateDropSequence(ASequence: TSequenceMIK): string; override;
    function GenerateEnableForeignKeys(AEnable: Boolean): string; override;
    function GenerateEnableTriggers(AEnable: Boolean): string; override;
  end;

implementation

{ TDDLSQLGeneratorSQLite }

function TDDLSQLGeneratorSQLite.BuilderPrimayKeyDefinition(ATable: TTableMIK): string;

  function GetPrimaryKeyColumns: string;
  var
    oColumn: TPair<string,TColumnMIK>;
  begin
    for oColumn in ATable.PrimaryKey.FieldsSort do
      Result := Result + oColumn.Value.Name + ', ';
    Result := Trim(Result);
    Delete(Result, Length(Result), 1);
  end;

begin
  Result := 'PRIMARY KEY(%s)';
  Result := Format(Result, [GetPrimaryKeyColumns]);
  Result := '  ' + Result;
end;

function TDDLSQLGeneratorSQLite.GenerateCreateSequence(ASequence: TSequenceMIK): string;
begin
  inherited;
  Result := 'INSERT INTO SQLITE_SEQUENCE (NAME, SEQ) VALUES (%s, %s);';
  Result := Format(Result, [QuotedStr(ASequence.Name), 0]);
end;

function TDDLSQLGeneratorSQLite.GenerateCreateTable(ATable: TTableMIK): string;
var
  oSQL: TStringBuilder;
  oColumn: TPair<string,TColumnMIK>;
begin
  oSQL := TStringBuilder.Create;
  Result := inherited GenerateCreateTable(ATable);
  try
    if ATable.Database.Schema <> '' then
      oSQL.Append(Format(Result, [ATable.Database.Schema + '.' + ATable.Name]))
    else
      oSQL.Append(Format(Result, [ATable.Name]));
    /// <summary>
    /// Add Colunas
    /// </summary>
    for oColumn in ATable.FieldsSort do
    begin
      oSQL.AppendLine;
      oSQL.Append('  ' + BuilderFieldDefinition(oColumn.Value));
      oSQL.Append(',');
    end;
    /// <summary>
    /// Add PrimariKey
    /// </summary>
    if ATable.PrimaryKey.Fields.Count > 0 then
    begin
      oSQL.AppendLine;
      oSQL.Append(BuilderPrimayKeyDefinition(ATable));
    end;
    /// <summary>
    /// Add ForeignKey
    /// </summary>
    if ATable.ForeignKeys.Count > 0 then
    begin
      oSQL.Append(',');
      oSQL.AppendLine;
      oSQL.Append(BuilderForeignKeyDefinition(ATable));
    end;
    /// <summary>
    /// Add Checks
    /// </summary>
    if ATable.Checks.Count > 0 then
    begin
      oSQL.Append(',');
      oSQL.AppendLine;
      oSQL.Append(BuilderCheckDefinition(ATable));
    end;
    oSQL.AppendLine;
    oSQL.Append(');');
    /// <summary>
    /// Add Indexe
    /// </summary>
    if ATable.IndexeKeys.Count > 0 then
    begin
      oSQL.AppendLine;
      oSQL.Append(BuilderIndexeDefinition(ATable));
    end;
    oSQL.AppendLine;
    Result := oSQL.ToString;
  finally
    oSQL.Free;
  end;
end;

function TDDLSQLGeneratorSQLite.GenerateDropSequence(ASequence: TSequenceMIK): string;
begin
  inherited;
  Result := 'DELETE FROM SQLITE_SEQUENCE WHERE NAME = %s;';
  Result := Format(Result, [QuotedStr(ASequence.Name)]);
end;

function TDDLSQLGeneratorSQLite.GenerateDropTable(ATable: TTableMIK): string;
begin
  Result := inherited GenerateDropTable(ATable);
end;

function TDDLSQLGeneratorSQLite.GenerateEnableForeignKeys(AEnable: Boolean): string;
begin
  if AEnable then
    Result := 'PRAGMA foreign_keys = on;'
  else
    Result := 'PRAGMA foreign_keys = off;';
end;

function TDDLSQLGeneratorSQLite.GenerateEnableTriggers(AEnable: Boolean): string;
begin

end;

initialization
  TSQLDriverRegister.GetInstance.RegisterDriver(dnSQLite, TDDLSQLGeneratorSQLite.Create);

end.
