{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.metadata.extract;

interface

uses
  DB,
  Generics.Collections,
  ormbr.factory.interfaces,
  ormbr.metadata.interfaces,
  ormbr.database.mapping,
  ormbr.mapping.repository,
  ormbr.types.database;

type
  TCatalogMetadataAbstract = class(TInterfacedObject, IDatabaseMetadata)
  private
    function GetConnection: IDBConnection;
    procedure SetConnection(const Value: IDBConnection);
    function GetCatalogMetadata: TCatalogMetadataMIK;
    procedure SetCatalogMetadata(const Value: TCatalogMetadataMIK);
  protected
    FSQLText: string;
    FConnection: IDBConnection;
    FCatalogMetadata: TCatalogMetadataMIK;
    function GetSelectTables: string; virtual; abstract;
    function GetSelectTableColumns(ATableName: string): string; virtual; abstract;
    function GetSelectPrimaryKey(ATableName: string): string; virtual; abstract;
    function GetSelectPrimaryKeyColumns(APrimaryKeyName: string): string; virtual; abstract;
    function GetSelectForeignKey(ATableName: string): string; virtual; abstract;
    function GetSelectIndexe(ATableName: string): string; virtual; abstract;
    function GetSelectIndexeColumns(AIndexeName: string): string; virtual; abstract;
    function GetSelectTriggers(ATableName: string): string; virtual; abstract;
    function GetSelectViews: string; virtual; abstract;
    function GetSelectSequences: string; virtual; abstract;
  public
    procedure GetCatalogs; virtual; abstract;
    procedure GetSchemas; virtual; abstract;
    procedure GetTables; virtual; abstract;
    procedure GetColumns(ATable: TTableMIK); virtual; abstract;
    procedure GetPrimaryKey(ATable: TTableMIK); virtual; abstract;
    procedure GetIndexeKeys(ATable: TTableMIK); virtual; abstract;
    procedure GetForeignKeys(ATable: TTableMIK); virtual; abstract;
    procedure GetChecks(ATable: TTableMIK); virtual; abstract;
    procedure GetTriggers(ATable: TTableMIK); virtual; abstract;
    procedure GetSequences; virtual; abstract;
    procedure GetProcedures; virtual; abstract;
    procedure GetFunctions; virtual; abstract;
    procedure GetViews; virtual; abstract;
    procedure GetDatabaseMetadata; virtual; abstract;
    property Connection: IDBConnection read GetConnection write SetConnection;
    property CatalogMetadata: TCatalogMetadataMIK read GetCatalogMetadata write SetCatalogMetadata;
  end;

  TModelMetadataAbstract = class(TInterfacedObject, IModelMetadata)
  private
    function GetConnection: IDBConnection;
    procedure SetConnection(const Value: IDBConnection);
  protected
    FFieldType: TDictionary<string, TFieldType>;
    FConnection: IDBConnection;
    FCatalogMetadata: TCatalogMetadataMIK;
  public
    constructor Create(ACatalogMetadata: TCatalogMetadataMIK); virtual;
    procedure GetCatalogs; virtual; abstract;
    procedure GetSchemas; virtual; abstract;
    procedure GetTables; virtual; abstract;
    procedure GetColumns(ATable: TTableMIK; AClass: TClass); virtual; abstract;
    procedure GetPrimaryKey(ATable: TTableMIK; AClass: TClass); virtual; abstract;
    procedure GetIndexeKeys(ATable: TTableMIK; AClass: TClass); virtual; abstract;
    procedure GetForeignKeys(ATable: TTableMIK; AClass: TClass); virtual; abstract;
    procedure GetChecks(ATable: TTableMIK; AClass: TClass); virtual; abstract;
    procedure GetSequences; virtual; abstract;
    procedure GetProcedures; virtual; abstract;
    procedure GetFunctions; virtual; abstract;
    procedure GetViews; virtual; abstract;
    procedure GetTriggers; virtual; abstract;
    procedure GetModelMetadata; virtual; abstract;
    property Connection: IDBConnection read GetConnection write SetConnection;
  end;

implementation

{ TCatalogMetadataAbstract }

function TCatalogMetadataAbstract.GetCatalogMetadata: TCatalogMetadataMIK;
begin
  Result := FCatalogMetadata;
end;

function TCatalogMetadataAbstract.GetConnection: IDBConnection;
begin
  Result := FConnection;
end;

procedure TCatalogMetadataAbstract.SetCatalogMetadata(const Value: TCatalogMetadataMIK);
begin
  FCatalogMetadata := Value;
end;

procedure TCatalogMetadataAbstract.SetConnection(const Value: IDBConnection);
begin
  FConnection := Value;
end;

{ TModelMetadataAbstract }

constructor TModelMetadataAbstract.Create(ACatalogMetadata: TCatalogMetadataMIK);
begin
  FCatalogMetadata := ACatalogMetadata;
end;

function TModelMetadataAbstract.GetConnection: IDBConnection;
begin
  Result := FConnection;
end;

procedure TModelMetadataAbstract.SetConnection(const Value: IDBConnection);
begin
  FConnection := Value;
end;

end.
