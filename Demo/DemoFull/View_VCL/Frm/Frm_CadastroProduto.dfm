inherited FrmCadastroProduto: TFrmCadastroProduto
  Caption = 'Cadastro de Produto'
  ClientHeight = 474
  ClientWidth = 702
  OnDestroy = FormDestroy
  ExplicitWidth = 718
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnTitulo: TPanel
    Width = 702
    ExplicitWidth = 702
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 25
    Width = 702
    Height = 449
    ActivePage = tabCLI_Detalhe
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object tabCLI_Pesquisa: TTabSheet
      Caption = 'Pesquisa'
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 694
        Height = 418
        Align = alClient
        DataSource = dsProduto_Pesquisa
        TabOrder = 0
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Arial'
        TitleFont.Style = []
      end
    end
    object tabCLI_Detalhe: TTabSheet
      Caption = 'Detalhe'
      ImageIndex = 1
      OnShow = tabCLI_DetalheShow
      object DBText1: TDBText
        Left = 144
        Top = 48
        Width = 65
        Height = 17
        DataField = 'produto_id'
        DataSource = dsProduto_Detalhe
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 88
        Top = 48
        Width = 49
        Height = 16
        Caption = 'Codigo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 86
        Top = 74
        Width = 51
        Height = 16
        Caption = 'Unidade:'
      end
      object Label3: TLabel
        Left = 75
        Top = 100
        Width = 62
        Height = 16
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 64
        Top = 129
        Width = 73
        Height = 16
        Caption = 'Embalagem:'
      end
      object Label5: TLabel
        Left = 99
        Top = 158
        Width = 38
        Height = 16
        Caption = 'Pre'#231'o:'
      end
      object DBEdit1: TDBEdit
        Left = 144
        Top = 71
        Width = 41
        Height = 24
        DataField = 'produto_unidade'
        DataSource = dsProduto_Detalhe
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 0
        Top = 361
        Width = 694
        Height = 57
        Align = alBottom
        BevelOuter = bvNone
        Color = clGray
        ParentBackground = False
        TabOrder = 1
        object DBNavigator1: TDBNavigator
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 688
          Height = 51
          DataSource = dsProduto_Detalhe
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]
          Align = alClient
          TabOrder = 0
        end
      end
      object DBEdit3: TDBEdit
        Left = 144
        Top = 155
        Width = 121
        Height = 24
        DataField = 'produto_preco'
        DataSource = dsProduto_Detalhe
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 145
        Top = 97
        Width = 312
        Height = 24
        DataField = 'produto_descricao'
        DataSource = dsProduto_Detalhe
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 144
        Top = 126
        Width = 312
        Height = 24
        DataField = 'produto_embalagem'
        DataSource = dsProduto_Detalhe
        TabOrder = 4
      end
    end
  end
  object dsProduto_Pesquisa: TDataSource
    AutoEdit = False
    DataSet = DtmPRO.Produto_Pesquisa
    Left = 618
    Top = 69
  end
  object dsProduto_Detalhe: TDataSource
    AutoEdit = False
    DataSet = DtmPRO.Produto_Detalhe
    Left = 618
    Top = 141
  end
end
