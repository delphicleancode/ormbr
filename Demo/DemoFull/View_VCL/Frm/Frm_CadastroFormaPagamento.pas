unit Frm_CadastroFormaPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Dtm_CadastroFormaPagamento,Frm_MDIChildInherit,
  Vcl.ExtCtrls, Data.DB, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls;

type
  TFrmCadastroFormaPagamento = class(TFrmMDIChildInherit)
    PageControl1: TPageControl;
    tabFPG_Pesquisa: TTabSheet;
    DBGrid1: TDBGrid;
    tabFPG_Detalhe: TTabSheet;
    DBText1: TDBText;
    Label1: TLabel;
    Label3: TLabel;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    dsFormaPagto_Pesquisa: TDataSource;
    dsFormaPagto_Detalhe: TDataSource;
    DBEdit2: TDBEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tabFPG_DetalheShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastroFormaPagamento: TFrmCadastroFormaPagamento;

implementation

{$R *.dfm}

procedure TFrmCadastroFormaPagamento.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TDtmFPG,DtmFPG);
  tabFPG_Pesquisa.Show;
end;

procedure TFrmCadastroFormaPagamento.FormDestroy(Sender: TObject);
begin
   FreeAndNil(DtmFPG);
   inherited;
end;

procedure TFrmCadastroFormaPagamento.tabFPG_DetalheShow(Sender: TObject);
begin
  inherited;
   DtmFPG.Open_Detalhe(DtmFPG.FormaPagto_Pesquisa.FieldByName('formapagto_id').AsInteger);
end;

end.
