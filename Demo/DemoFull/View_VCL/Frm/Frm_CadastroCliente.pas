unit Frm_CadastroCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls,
  Dtm_CadastroCliente, Data.DB, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls, Frm_MDIChildInherit;

type
  TFrmCadastroCliente = class(TFrmMDIChildInherit)
    dsCliente_Pesquisa: TDataSource;
    PageControl1: TPageControl;
    tabCLI_Pesquisa: TTabSheet;
    tabCLI_Detalhe: TTabSheet;
    DBGrid1: TDBGrid;
    DBEdit1: TDBEdit;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    dsCliente_Detalhe: TDataSource;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBMemo1: TDBMemo;
    Label6: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure tabCLI_DetalheShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastroCliente: TFrmCadastroCliente;

implementation

{$R *.dfm}


procedure TFrmCadastroCliente.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TDtmCLI,DtmCLI);
  tabCLI_Pesquisa.Show;
end;

procedure TFrmCadastroCliente.FormDestroy(Sender: TObject);
begin
   FreeAndNil(DtmCLI);
   inherited;
end;

procedure TFrmCadastroCliente.tabCLI_DetalheShow(Sender: TObject);
begin
   DtmCLI.Open_Detalhe(DtmCLI.Cliente_Pesquisa.FieldByName('Cliente_id').AsInteger);
end;



end.

