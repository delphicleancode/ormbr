unit Frm_MDIChildInherit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls;

type
  TFrmMDIChildInherit = class(TForm)
    pnTitulo: TPanel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMDIChildInherit: TFrmMDIChildInherit;

implementation

{$R *.dfm}

procedure TFrmMDIChildInherit.FormCreate(Sender: TObject);
begin
   Self.pnTitulo.Caption := Self.Caption;
end;

procedure TFrmMDIChildInherit.FormShow(Sender: TObject);
begin
   Self.WindowState := wsMaximized;
end;

end.
