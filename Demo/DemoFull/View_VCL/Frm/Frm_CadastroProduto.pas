unit Frm_CadastroProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Dtm_CadastroProduto,Frm_MDIChildInherit,
  Vcl.ExtCtrls, Data.DB, Vcl.DBCtrls, Vcl.StdCtrls, Vcl.Mask, Vcl.Grids,
  Vcl.DBGrids, Vcl.ComCtrls;

type
  TFrmCadastroProduto = class(TFrmMDIChildInherit)
    PageControl1: TPageControl;
    tabCLI_Pesquisa: TTabSheet;
    DBGrid1: TDBGrid;
    tabCLI_Detalhe: TTabSheet;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    DBEdit3: TDBEdit;
    dsProduto_Pesquisa: TDataSource;
    dsProduto_Detalhe: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tabCLI_DetalheShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCadastroProduto: TFrmCadastroProduto;

implementation

{$R *.dfm}

procedure TFrmCadastroProduto.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TDtmPRO,DtmPRO);
  tabCLI_Pesquisa.Show;
end;

procedure TFrmCadastroProduto.FormDestroy(Sender: TObject);
begin
   FreeAndNil(DtmPRO);
   inherited;
end;

procedure TFrmCadastroProduto.tabCLI_DetalheShow(Sender: TObject);
begin
  inherited;
   DtmPRO.Open_Detalhe(DtmPRO.Produto_Pesquisa.FieldByName('Produto_id').AsInteger);
end;

end.
