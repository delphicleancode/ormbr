unit Frm_MovimentoVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Mask,
  Vcl.DBCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Imaging.pngimage,
  Dtm_MovimentoVenda,Frm_MDIChildInherit, Vcl.Buttons;

type
  TFrmMovimentoVenda = class(TFrmMDIChildInherit)
    dsVenda_Pesquisa: TDataSource;
    dsLookFind_cliente: TDataSource;
    dsLookup_Formapagamento: TDataSource;
    dsLookup_produto: TDataSource;
    PageControl1: TPageControl;
    tabVND_Pesquisa: TTabSheet;
    DBGrid1: TDBGrid;
    tabVND_Detalhe: TTabSheet;
    Panel2: TPanel;
    DBNavigator1: TDBNavigator;
    Panel1: TPanel;
    DBGrid2: TDBGrid;
    Panel3: TPanel;
    Label6: TLabel;
    SpeedButton2: TSpeedButton;
    DBText3: TDBText;
    Label7: TLabel;
    Label8: TLabel;
    edtProduto_id: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBNavigator2: TDBNavigator;
    Panel4: TPanel;
    DBText1: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    DBText2: TDBText;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    DBEdit4: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBMemo1: TDBMemo;
    dsVenda: TDataSource;
    dsVendaItem: TDataSource;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tabVND_DetalheShow(Sender: TObject);

  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  FrmMovimentoVenda: TFrmMovimentoVenda;

implementation

{$R *.dfm}

procedure TFrmMovimentoVenda.FormCreate(Sender: TObject);
begin
  inherited;
  Application.CreateForm(TDtmMVD,DtmMVD);
  tabVND_Pesquisa.Show;


end;

procedure TFrmMovimentoVenda.FormDestroy(Sender: TObject);
begin
  FreeAndNil(DtmMVD);
  inherited;
end;



procedure TFrmMovimentoVenda.tabVND_DetalheShow(Sender: TObject);
begin
  inherited;
   DtmMVD.Open_Detalhe(DtmMVD.Venda_Pesquisa.FieldByName('venda_id').AsInteger);
end;

end.
