unit Dtm_CadastroFormaPagamento;

interface

uses
  System.SysUtils, System.Classes, ormbr.factory.interfaces, dialogs,
  ormbr.dependency.interfaces, Data.DB, Datasnap.DBClient,
  Dtm_Connection,
  ormbr.criteria,
  ormbr.dependency.injection.clientdataset,
  demo.controller.base,
  demo.model.formapagamento,
  demo.controller.formapagamento;

type
  TDtmFPG = class(TDataModule)
    FormaPagto_Pesquisa: TClientDataSet;
    FormaPagto_Detalhe: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure FormaPagto_DetalheAfterPost(DataSet: TDataSet);
    procedure FormaPagto_DetalheAfterDelete(DataSet: TDataSet);
    procedure FormaPagto_DetalheNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    oFormaPagto_Pesquisa: IContainerDataSet<Tformapagamento>;
    oFormaPagto_Detalhe: IContainerDataSet<Tformapagamento>;
    oControllerFormaPagto: TControllerFormapagamento<Tformapagamento>;
    //
    procedure Open_Detalhe(pid:Integer);
  end;

var
  DtmFPG: TDtmFPG;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDtmFPG.FormaPagto_DetalheAfterDelete(DataSet: TDataSet);
begin
  oFormaPagto_Detalhe.DataSet.ApplyUpdates(0);
  //
  oFormaPagto_Pesquisa.DataSet.Close;
  oFormaPagto_Pesquisa.DataSet.Open;
end;

procedure TDtmFPG.FormaPagto_DetalheAfterPost(DataSet: TDataSet);
begin
  oFormaPagto_Detalhe.DataSet.ApplyUpdates(0);
  //
  oFormaPagto_Pesquisa.DataSet.Close;
  oFormaPagto_Pesquisa.DataSet.Open;
end;

procedure TDtmFPG.FormaPagto_DetalheNewRecord(DataSet: TDataSet);
begin
//  FormaPagto_Detalhe.FieldByName('formapagto_id').AsInteger :=  oControllerFormaPagto.GetSequencia('formapagamento','formapagto_id');

  // Linhas Incluidas para Facilitar os cadastros nos testes
  FormaPagto_Detalhe.FieldByName('formapagto_descricao').AsString := 'Forma Pagamento '+IntToStr(FormaPagto_Detalhe.FieldByName('formapagto_id').AsInteger);
end;

procedure TDtmFPG.DataModuleCreate(Sender: TObject);
begin
  /// Instancia todos os objetos Modelo e Link ao ClientDataSet no Create;
  /// Class Adapter
  /// Parāmetros: (IDBConnection, TClientDataSet, PageSize)
  oFormaPagto_Pesquisa := TContainerClientDataSet<Tformapagamento>.Create(DtmCONN.oConn, FormaPagto_Pesquisa);
  oFormaPagto_Detalhe  := TContainerClientDataSet<Tformapagamento>.Create(DtmCONN.oConn, FormaPagto_Detalhe,1);
  //
  oControllerFormaPagto := TControllerFormapagamento<Tformapagamento>.Create(DtmCONN.oConn, oFormaPagto_Detalhe.DataSet);

  ///Utiliza Objeto Modelo Instanciado
  oFormaPagto_Pesquisa.DataSet.Open;
end;


procedure TDtmFPG.Open_Detalhe(pid: Integer);
begin
  oFormaPagto_Detalhe.DataSet.Open(pid);
end;

end.
