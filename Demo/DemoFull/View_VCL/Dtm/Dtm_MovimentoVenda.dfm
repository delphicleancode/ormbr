object DtmMVD: TDtmMVD
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 170
  Width = 384
  object Venda: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterPost = VendaAfterPost
    BeforeCancel = VendaBeforeCancel
    OnNewRecord = VendaNewRecord
    AfterApplyUpdates = VendaAfterApplyUpdates
    Left = 42
    Top = 21
  end
  object Venda_Item: TClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = Venda_ItemNewRecord
    Left = 38
    Top = 78
  end
  object Venda_Pesquisa: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 288
    Top = 23
  end
  object LookFind_Cliente: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 130
    Top = 22
  end
  object Lookup_Formapagamento: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 284
    Top = 82
  end
  object Lookup_Produto: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 130
    Top = 80
  end
  object contareceber: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 214
    Top = 23
  end
end
