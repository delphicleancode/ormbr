unit Dtm_CadastroProduto;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Datasnap.DBClient,
  ormbr.dependency.interfaces, Dtm_Connection,
  ormbr.dependency.injection.clientdataset,
  demo.model.produto, demo.controller.produto;

type
  TDtmPRO = class(TDataModule)
    Produto_Pesquisa: TClientDataSet;
    Produto_Detalhe: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure Produto_DetalheAfterPost(DataSet: TDataSet);
    procedure Produto_DetalheAfterDelete(DataSet: TDataSet);
    procedure Produto_DetalheNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    oProduto_Pesquisa: IContainerDataSet<Tproduto>;
    oProduto_Detalhe: IContainerDataSet<Tproduto>;
    oControllerProduto: TControllerProduto<Tproduto>;
    //
    procedure Open_Detalhe(pid:Integer);
  end;

var
  DtmPRO: TDtmPRO;

implementation

uses
  ormbr.criteria;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDtmPRO.DataModuleCreate(Sender: TObject);
begin
  /// Instancia todos os objetos Modelo e Link ao ClientDataSet no Create;
  /// Class Adapter
  /// Parāmetros: (IDBConnection, TClientDataSet, PageSize)
  oProduto_Pesquisa := TContainerClientDataSet<Tproduto>.Create(DtmCONN.oConn, produto_Pesquisa);
  oProduto_Detalhe  := TContainerClientDataSet<Tproduto>.Create(DtmCONN.oConn, produto_Detalhe,1);

  oControllerProduto := TControllerProduto<Tproduto>.Create(DtmCONN.oConn, oProduto_Detalhe.DataSet);

  ///Utiliza Objeto Modelo Instanciado
  oProduto_Pesquisa.DataSet.Open(CreateCriteria.Select.All.From('Produto').OrderBy('produto_id'));
end;

procedure TDtmPRO.Open_Detalhe(pid: Integer);
begin
  oProduto_Detalhe.DataSet.Open(pid);
end;

procedure TDtmPRO.Produto_DetalheAfterDelete(DataSet: TDataSet);
begin
  oProduto_Detalhe.DataSet.ApplyUpdates(0);
  //
  oProduto_Pesquisa.DataSet.Close;
  oProduto_Pesquisa.DataSet.Open;
end;

procedure TDtmPRO.Produto_DetalheAfterPost(DataSet: TDataSet);
begin
  oProduto_Detalhe.DataSet.ApplyUpdates(0);
  //
  oProduto_Pesquisa.DataSet.Close;
  oProduto_Pesquisa.DataSet.Open;
end;

procedure TDtmPRO.Produto_DetalheNewRecord(DataSet: TDataSet);
begin
//  Produto_Detalhe.FieldByName('produto_id').AsInteger := oControllerProduto.GetSequencia('produto','produto_id');

  // Linhas Incluidas para Facilitar os cadastros nos testes
  Produto_Detalhe.FieldByName('produto_unidade').AsString := 'UN';
  Produto_Detalhe.FieldByName('produto_descricao').AsString := 'Produto '+IntToStr(Produto_Detalhe.FieldByName('produto_id').AsInteger);
  Produto_Detalhe.FieldByName('produto_preco').AsCurrency := (10 + Produto_Detalhe.FieldByName('produto_id').AsInteger);
end;

end.
