unit Dtm_MovimentoVenda;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Datasnap.DBClient,Dialogs,
  ormbr.dependency.interfaces,
  ormbr.criteria,
  Dtm_Connection,
  ormbr.dependency.injection.clientdataset,
  demo.model.venda,
  demo.model.vendaitem,
  demo.model.produto,
  demo.model.cliente,
  demo.model.formapagamento,
  demo.controller.venda,
  demo.model.contareceber,
  demo.controller.contareceber;


type
  TDtmMVD = class(TDataModule)
    Venda: TClientDataSet;
    Venda_Item: TClientDataSet;
    Venda_Pesquisa: TClientDataSet;
    LookFind_Cliente: TClientDataSet;
    Lookup_Formapagamento: TClientDataSet;
    Lookup_Produto: TClientDataSet;
    contareceber: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure VendaAfterPost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure VendaNewRecord(DataSet: TDataSet);
    procedure Venda_ItemNewRecord(DataSet: TDataSet);
    procedure VendaBeforeCancel(DataSet: TDataSet);
    procedure VendaAfterApplyUpdates(Sender: TObject;
      var OwnerData: OleVariant);
  private
    { Private declarations }
    procedure Venda_cliente_idSetText(Sender: TField; const Text: string);
  public
    { Public declarations }
    oVenda_Pesquisa: IContainerDataSet<Tvenda>;
    oVenda: IContainerDataSet<Tvenda>;
    oVenda_Itens: IContainerDataSet<Tvendaitem>;
    oContareceber: IContainerDataSet<Tcontareceber>;
    oLookFind_Cliente: IContainerDataSet<Tcliente>;
    oLookup_Formapagamento: IContainerDataSet<Tformapagamento>;
    oLookup_Produto: IContainerDataSet<Tproduto>;
    oController_Venda: TControllerVenda<Tvenda>;
    oController_ContaReceber: TControllerContareceber<Tcontareceber>;

    procedure Open_Detalhe(pid:Integer);
  end;

var
  DtmMVD: TDtmMVD;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Frm_MovimentoVenda;

{$R *.dfm}

procedure TDtmMVD.DataModuleCreate(Sender: TObject);
begin
  /// Class Adapter
  /// Par�metros: (IDBConnection, TClientDataSet)
  oVenda_Pesquisa := TContainerClientDataSet<Tvenda>.Create(DtmCONN.oConn, Venda_Pesquisa,10);

  /// Par�metros: (IDBConnection, TClientDataSet)
  oVenda := TContainerClientDataSet<Tvenda>.Create(DtmCONN.oConn, Venda,1);

  /// Master-Detail
  oVenda_Itens := TContainerClientDataSet<Tvendaitem>.Create(DtmCONN.oConn, Venda_Item,oVenda.DataSet);

  // LookFind Cliente
  oLookFind_Cliente := TContainerClientDataSet<Tcliente>.Create(DtmCONN.oConn, LookFind_Cliente,1,oVenda.DataSet);

  // LookFind FormaPagamento
  oLookup_Formapagamento := TContainerClientDataSet<Tformapagamento>.Create(DtmCONN.oConn, Lookup_Formapagamento);
  oVenda.DataSet.AddLookupField('FormaPagamento','formapagto_id',oLookup_Formapagamento.DataSet,'formapagto_id','formapagto_descricao');

  /// Lookup lista de registro (DBLookupComboBox)
  oLookup_Produto := TContainerClientDataSet<Tproduto>.Create(DtmCONN.oConn, Lookup_Produto);
  oVenda_Itens.DataSet.AddLookupField('produto_descricao','produto_id',oLookup_Produto.DataSet,'produto_id','produto_descricao');

    /// Par�metros: (IDBConnection, TClientDataSet)
  oContareceber := TContainerClientDataSet<Tcontareceber>.Create(DtmCONN.oConn, contareceber);

  //Cria��o dos Controles
  oController_Venda := TControllerVenda<Tvenda>.Create(DtmCONN.oConn, oVenda.DataSet);
  oController_ContaReceber := TControllerContareceber<Tcontareceber>.Create(DtmCONN.oConn, oContareceber.DataSet);

  oVenda_Pesquisa.DataSet.Open;

 // TIntegerField(Venda.FieldByName('cliente_id')).OnSetText := Venda_cliente_idSetText;
end;

procedure TDtmMVD.DataModuleDestroy(Sender: TObject);
begin
   oController_Venda.Free;
   oController_ContaReceber.Free;
end;

procedure TDtmMVD.Open_Detalhe(pid: Integer);
begin
  oVenda.DataSet.Open(pid);
//  oVenda_Itens.DataSet.Open;
  oVenda_Itens.DataSet.Open(CreateCriteria.Select.All.From('vendaitem').Where('venda_id = '+IntToStr(pid)));

//  oLookup_Formapagamento.DataSet.Open;
  oLookFind_Cliente.DataSet.Open;
end;

procedure TDtmMVD.VendaAfterApplyUpdates(Sender: TObject;
  var OwnerData: OleVariant);
begin
  // Tudo que estiver dentro desse evento estara dentro de uma transa��o
  //
  oController_Venda.Gera_ContaReceberVenda(Venda.FieldByName('venda_id').AsInteger);
end;

procedure TDtmMVD.VendaAfterPost(DataSet: TDataSet);
begin

   Venda_Item.DisableControls;
   while not Venda_Item.Eof do
   begin
     Venda_Item.Edit;
     Venda_Item.FieldByName('venda_id').AsInteger :=  Venda.FieldByName('venda_id').AsInteger;
     Venda_Item.Post;
//
     Venda_Item.Next;
   end;
   Venda_Item.EnableControls;

   oVenda.DataSet.ApplyUpdates(0);
   oVenda_Itens.DataSet.ApplyUpdates(0);

   oVenda_Pesquisa.DataSet.Close;
   oVenda_Pesquisa.DataSet.Open;
end;

procedure TDtmMVD.VendaBeforeCancel(DataSet: TDataSet);
begin
   Venda_Item.Cancel;
end;

procedure TDtmMVD.VendaNewRecord(DataSet: TDataSet);
begin
   //Venda
   Venda.FieldByName('venda_datalancamento').AsDateTime  := date;
   Venda.FieldByName('venda_dataalteracao').AsDateTime  := date;

end;

procedure TDtmMVD.Venda_cliente_idSetText(Sender: TField; const Text: string);
begin
//  oLookFind_Cliente.DataSet.Close;
//  oLookFind_Cliente.DataSet.Open;
 // ShowMessage('set');
end;

procedure TDtmMVD.Venda_ItemNewRecord(DataSet: TDataSet);
begin
  frmMovimentoVenda.edtProduto_id.Setfocus;

  if Venda.State in [dsEdit,dsInsert] then
  begin
     FrmMovimentoVenda.edtProduto_id.SetFocus;
  end
  else
  begin
    Venda_Item.Cancel;
    ShowMessage('Favor entrar e mode de Edi��o ou Inser��o..');
  end;
end;

end.
