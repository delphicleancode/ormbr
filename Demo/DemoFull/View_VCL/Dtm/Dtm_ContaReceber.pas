unit Dtm_ContaReceber;

interface

uses
  System.SysUtils, System.Classes, Data.DB, Datasnap.DBClient,
  ormbr.dependency.interfaces, Dtm_Connection,
  ormbr.dependency.injection.clientdataset,
  demo.model.contareceber,
  demo.controller.contareceber;

type
  TDtmCRE = class(TDataModule)
    Contareceber_Pesquisa: TClientDataSet;
    Contareceber_Detalhe: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure Contareceber_DetalheAfterPost(DataSet: TDataSet);
    procedure Contareceber_DetalheAfterDelete(DataSet: TDataSet);
    procedure Contareceber_DetalheNewRecord(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    oContaReceber_Pesquisa: IContainerDataSet<Tcontareceber>;
    oContaReceber_Detalhe: IContainerDataSet<Tcontareceber>;
    oController_ContaReceber: TControllerContareceber<Tcontareceber>;
    //
    procedure Open_Detalhe(pid:Integer);

  end;

var
  DtmCRE: TDtmCRE;

implementation

uses
  ormbr.criteria;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDtmCRE.DataModuleCreate(Sender: TObject);
begin
  /// Instancia todos os objetos Modelo e Link ao ClientDataSet no Create;
  /// Class Adapter
  /// Parāmetros: (IDBConnection, TClientDataSet, PageSize)
  oContaReceber_Pesquisa := TContainerClientDataSet<Tcontareceber>.Create(DtmCONN.oConn, Contareceber_Pesquisa);
  oContaReceber_Detalhe  := TContainerClientDataSet<Tcontareceber>.Create(DtmCONN.oConn, Contareceber_Detalhe,1);

  oController_ContaReceber := TControllerContareceber<Tcontareceber>.Create(DtmCONN.oConn, oContaReceber_Detalhe.DataSet);


  ///Utiliza Objeto Modelo Instanciado
  oContaReceber_Pesquisa.DataSet.Open(CreateCriteria.Select.All.From('contareceber').OrderBy('contareceber_id'));
end;

procedure TDtmCRE.DataModuleDestroy(Sender: TObject);
begin
  oController_ContaReceber.Free;
end;

procedure TDtmCRE.Open_Detalhe(pid: Integer);
begin
  oContaReceber_Detalhe.DataSet.Open(pid);
end;

procedure TDtmCRE.Contareceber_DetalheAfterDelete(DataSet: TDataSet);
begin
  oContaReceber_Detalhe.DataSet.ApplyUpdates(0);
  //
  oContaReceber_Pesquisa.DataSet.Close;
  oContaReceber_Pesquisa.DataSet.Open;
end;

procedure TDtmCRE.Contareceber_DetalheAfterPost(DataSet: TDataSet);
begin
  oContaReceber_Detalhe.DataSet.ApplyUpdates(0);
  //
  oContaReceber_Pesquisa.DataSet.Close;
  oContaReceber_Pesquisa.DataSet.Open;
end;

procedure TDtmCRE.Contareceber_DetalheNewRecord(DataSet: TDataSet);
begin
//  Contareceber_Detalhe.FieldByName('contareceber_id').AsInteger := oController_ContaReceber.GetSequencia('contareceber','contareceber_id');

  // Linhas Incluidas para Facilitar os cadastros nos testes
  Contareceber_Detalhe.FieldByName('contareceber_datalancamento').AsDateTime := Date;
  Contareceber_Detalhe.FieldByName('contareceber_datavencimento').AsDateTime := (Date+30);
end;

end.
