unit Dtm_CadastroCliente;

interface

uses
  System.SysUtils, System.Classes, ormbr.factory.interfaces, dialogs,
  ormbr.dependency.interfaces, Data.DB, Datasnap.DBClient,
  Dtm_Connection,
  ormbr.dependency.injection.clientdataset,
  demo.controller.base,
  demo.model.cliente,
  demo.controller.cliente,
  ormbr.criteria;

type
  TDtmCLI = class(TDataModule)
    Cliente_Pesquisa: TClientDataSet;
    Cliente_Detalhe: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure Cliente_DetalheAfterPost(DataSet: TDataSet);
    procedure Cliente_DetalheAfterDelete(DataSet: TDataSet);
    procedure Cliente_DetalheNewRecord(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    oCliente_Pesquisa: IContainerDataSet<Tcliente>;
    oCliente_Detalhe: IContainerDataSet<Tcliente>;
    oController_Cliente: TControllerCliente<Tcliente>;
    //
    procedure Open_Detalhe(pid:Integer);
  end;

var
  DtmCLI: TDtmCLI;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDtmCLI.Cliente_DetalheAfterDelete(DataSet: TDataSet);
begin
  oCliente_Detalhe.DataSet.ApplyUpdates(0);
  //
  oCliente_Pesquisa.DataSet.Close;
  oCliente_Pesquisa.DataSet.Open;
end;

procedure TDtmCLI.Cliente_DetalheAfterPost(DataSet: TDataSet);
begin
  oCliente_Detalhe.DataSet.ApplyUpdates(0);
  //
  oCliente_Pesquisa.DataSet.Close;
  oCliente_Pesquisa.DataSet.Open;
end;

procedure TDtmCLI.Cliente_DetalheNewRecord(DataSet: TDataSet);
begin
//  Cliente_Detalhe.FieldByName('cliente_id').AsInteger :=  oController_Cliente.GetSequencia('cliente','cliente_id');

  // Linhas Incluidas para Facilitar os cadastros nos testes
  Cliente_Detalhe.FieldByName('cliente_nome').AsString := 'Nome do Cliente '+IntToStr(Cliente_Detalhe.FieldByName('cliente_id').AsInteger);
  Cliente_Detalhe.FieldByName('cliente_endereco').AsString := 'Endereco '+IntToStr(Cliente_Detalhe.FieldByName('cliente_id').AsInteger);
  Cliente_Detalhe.FieldByName('cliente_cidade').AsString := 'Cidade '+IntToStr(Cliente_Detalhe.FieldByName('cliente_id').AsInteger);
  Cliente_Detalhe.FieldByName('cliente_telefone').AsString := '2760706070';
  Cliente_Detalhe.FieldByName('cliente_observacao').AsString := 'Oberservacao '+IntToStr(Cliente_Detalhe.FieldByName('cliente_id').AsInteger);
end;

procedure TDtmCLI.DataModuleCreate(Sender: TObject);
begin
  /// Instancia todos os objetos Modelo e Link ao ClientDataSet no Create;
  /// Class Adapter
  /// Parāmetros: (IDBConnection, TClientDataSet, PageSize)
  oCliente_Detalhe  := TContainerClientDataSet<Tcliente>.Create(DtmCONN.oConn, Cliente_Detalhe);
  //
  oController_Cliente := TControllerCliente<Tcliente>.Create(DtmCONN.oConn, oCliente_Detalhe.DataSet);

  ///Utiliza Objeto Modelo Instanciado
  oCliente_Pesquisa := TContainerClientDataSet<Tcliente>.Create(DtmCONN.oConn, Cliente_Pesquisa);
  oCliente_Pesquisa.DataSet.Open(CreateCriteria.Select.All.From('Cliente').OrderBy('cliente_id'));
end;

procedure TDtmCLI.DataModuleDestroy(Sender: TObject);
begin
   oController_Cliente.Free;
end;

procedure TDtmCLI.Open_Detalhe(pid: Integer);
begin
  oCliente_Detalhe.DataSet.Open(pid);
end;

end.
