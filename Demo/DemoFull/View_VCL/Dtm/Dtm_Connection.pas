unit Dtm_Connection;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client

  ,ormbr.factory.interfaces  // Unit Interface Fabrica de Conex�o com Banco de Dados
  ,ormbr.factory.firedac, ormbr.criteria, FireDAC.Comp.UI     // Unit Fabrica de Conex�o com FireDAC
  ;

type
  TDtmCONN = class(TDataModule)
    FDConnection1: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    oConn: IDBConnection;
    function MaxCodigo(pTabela,pField:String): Integer;
  end;

var
  DtmCONN: TDtmCONN;

implementation

uses
  ormbr.types.database;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TDtmCONN.DataModuleCreate(Sender: TObject);
begin
  with FDConnection1 do
  begin
    DriverName := 'SQLite';
    ConnectionName := 'ORMBrSQLLite';
    Params.Clear;
    Params.Add('DriverID=SQLite');
    Params.Add('Database=.\BD_SQLite\database.db3');
  end;
  // Inst�ncia da class de conex�o via FireDAC
  oConn := TFactoryFireDAC.Create(FDConnection1, dnSQLite);
end;

function TDtmCONN.MaxCodigo(pTabela, pField: String): Integer;
var
  SQL : IDBResultSet;
begin
   SQL := DtmCONN.oConn.ExecuteSQL(CreateCriteria.Select('max('+pField+') ID').From(pTabela).AsString);
   try
     Result := (SQL.GetFieldValue('ID')+1);
   except
      Result := 1;
   end;
end;

end.
