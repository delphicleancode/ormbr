unit demo.controller.formapagamento;

interface

uses
  ormbr.bind.base,
  demo.controller.base,
  ormbr.factory.interfaces;

type
  TControllerFormapagamento<M: class, constructor> = class(TControllerBaseDemo<M>)
  public
    constructor Create(AConnection: IDBConnection; ADataSet: TDatasetBase<M>); override;
    destructor Destroy; override;

  end;

implementation


{ TControllerCliente<M> }

constructor TControllerFormapagamento<M>.Create(AConnection: IDBConnection;
  ADataSet: TDatasetBase<M>);
begin
  inherited;
   //
end;

destructor TControllerFormapagamento<M>.Destroy;
begin
  //
  inherited;
end;

end.
