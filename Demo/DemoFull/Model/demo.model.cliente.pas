unit demo.model.cliente;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.mapping,
  ormbr.types.lazy;

type
  [Entity]
  [Table('cliente','')]
  [PrimaryKey('cliente_id', AutoInc)]
  [Sequence('SEQ_CLIENTE')]
  Tcliente = class
  private
    { Private declarations }
    Fcliente_id: Integer;
    Fcliente_nome: String;
    Fcliente_endereco: String;
    Fcliente_cidade: String;
    Fcliente_telefone: String;
    Fcliente_observacao:String;
  public
    { Public declarations }
    [Restrictions([NotNull])]
    [Column('cliente_id', ftInteger)]
    [Dictionary('C�digo','Mensagem de valida��o','-1','','',taCenter)]
    property cliente_id: Integer Index 0 read Fcliente_id write Fcliente_id;

    [Column('cliente_nome', ftString)]
    [Dictionary('Nome Cliente','Mensagem de valida��o','','','',taLeftJustify)]
    property cliente_nome: String Index 1 read Fcliente_nome write Fcliente_nome;

    [Column('cliente_endereco', ftString)]
    [Dictionary('Endere�o','Mensagem de valida��o','','','',taLeftJustify)]
    property cliente_endereco: String Index 2 read Fcliente_endereco write Fcliente_endereco;

    [Column('cliente_cidade', ftString)]
    [Dictionary('Cidade','Mensagem de valida��o','','','',taLeftJustify)]
    property cliente_cidade: String Index 3 read Fcliente_cidade write Fcliente_cidade;

    [Column('cliente_telefone', ftString)]
    [Dictionary('Telefone','Mensagem de valida��o','','','',taLeftJustify)]
    property cliente_telefone: String Index 4 read Fcliente_telefone write Fcliente_telefone;

    [Column('cliente_observacao', ftString)]
    [Dictionary('Observa��o','Mensagem de valida��o','','','',taLeftJustify)]
    property cliente_observacao: String Index 5 read Fcliente_observacao write Fcliente_observacao;
  end;

implementation

end.
