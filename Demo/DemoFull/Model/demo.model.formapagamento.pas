unit demo.model.formapagamento;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.mapping,
  ormbr.types.lazy;

type
  [Entity]
  [Table('formapagamento','')]
  [PrimaryKey('formapagto_id', AutoInc)]
  [Sequence('SEQ_FORMASPAGAMENTO')]
  Tformapagamento = class
  private
    { Private declarations }
    Fformapagto_id: Integer;
    Fformapagto_descricao: String;
  public
    { Public declarations }
    [Column('formapagto_id', ftInteger)]
    [Dictionary('C�digo','Mensagem de valida��o','','','',taCenter)]
    property formapagto_id: Integer Index 0 read Fformapagto_id write Fformapagto_id;

    [Column('formapagto_descricao', ftString)]
    [Dictionary('Descri��o','Mensagem de valida��o','','','',taLeftJustify)]
    property formapagto_descricao: String Index 1 read Fformapagto_descricao write Fformapagto_descricao;
  end;

implementation

end.
