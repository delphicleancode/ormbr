unit demo.model.venda;

interface

uses
  Classes,
  DB,
  SysUtils,
  Generics.Collections,
  /// orm
  ormbr.mapping.attributes,
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  demo.model.vendaitem,
  demo.model.cliente,
  ormbr.types.mapping,
  demo.model.formapagamento,
  demo.model.contareceber;


type
  [Entity]
  [Table('venda','')]
  [PrimaryKey('venda_Id', AutoInc)]
  [Sequence('SEQ_VENDA')]
  Tvenda = class
  private
    { Private declarations }
    Fvenda_Id: Integer;
    Fvenda_datalancamento: TDateTime;
    Fvenda_observacao: Nullable<String>;
    Fvenda_dataalteracao: TDateTime;
    Fcliente_id: Nullable<Integer>;
    Fformapagto_id: Nullable<Integer>;
    Fvendaitem: TObjectList<Tvendaitem>;
    Fcliente: Tcliente;
    Fformapagamento: Tformapagamento;
    Fcontareceber : Tcontareceber;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;

    [Restrictions([NotNull])]
    [Column('venda_Id', ftInteger)]
    [Dictionary('venda_Id','Mensagem de valida��o','','','',taCenter)]
    property venda_Id: Integer Index 0 read Fvenda_Id write Fvenda_Id;

    [Column('venda_observacao', ftString)]
    [Dictionary('venda_observacao','Mensagem de valida��o','','','',taLeftJustify)]
    property venda_observacao: Nullable<String> Index 1 read Fvenda_observacao write Fvenda_observacao;

    [Column('venda_datalancamento', ftDateTime)]
    [Dictionary('venda_datalancamento','Mensagem de valida��o','','','',taCenter)]
    property venda_datalancamento: TDateTime Index 2 read Fvenda_datalancamento write Fvenda_datalancamento;

    [Column('venda_dataalteracao', ftDateTime)]
    [Dictionary('venda_dataalteracao','Mensagem de valida��o','','','',taCenter)]
    property venda_dataalteracao: TDateTime Index 3 read Fvenda_dataalteracao write Fvenda_dataalteracao;

    [Column('cliente_id', ftInteger)]
    [Dictionary('cliente_id','Mensagem de valida��o','','','',taCenter)]
    property cliente_id: Nullable<Integer> Index 4 read Fcliente_id write Fcliente_id;

    [Column('formapagto_id', ftInteger)]
    [Dictionary('formapagto_id','Mensagem de valida��o','','','',taCenter)]
    property formapagto_id: Nullable<Integer> Index 5 read Fformapagto_id write Fformapagto_id;


//    [Column('cliente_nome', ftString, 60)]
//    [Dictionary('Nome do Cliente')]
//    [JoinColumn('cliente_nome','cliente','cliente_id',LeftJoin)]
//    [Restrictions([NoInsert, NoUpdate])]
//    property cliente_nome: string index 5 read fcliente_nome write fcliente_nome;

//    [ForeignKey('cliente', None, None)]
//    [Association(OneToOne,'cliente_id','cliente_id')]
//    property cliente: Tcliente read Fcliente write Fcliente;

    [ForeignKey('formapagamento', None, None)]
    property formapagamento: Tformapagamento read Fformapagamento write Fformapagamento;

    [ForeignKey('vendaitem', Cascade, Cascade)]
    [Association(OneToMany,'venda_id','venda_id')]
    property vendaitem: TObjectList<Tvendaitem> read Fvendaitem write Fvendaitem;

    property produto: Tcontareceber read Fcontareceber write Fcontareceber;
  end;

implementation

{ Tvenda }

constructor Tvenda.Create;
begin
   Fvendaitem := TObjectList<Tvendaitem>.Create;
   Fcliente := Tcliente.Create;
   Fformapagamento := Tformapagamento.Create;
   Fcontareceber := Tcontareceber.Create;
end;

destructor Tvenda.Destroy;
begin
  Fvendaitem.Free;
  Fcliente.Free;
  Fformapagamento.Free;
  Fcontareceber.Free;
  inherited;
end;

end.
