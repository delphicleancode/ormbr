unit ormbr.model.detail;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  ormbr.model.lookup,
  ormbr.mapping.register;

type
  [Entity]
  [Table('detail','')]
  [PrimaryKey('detail_id; master_id', 'Chave prim�ria')]
  Tdetail = class
  private
    { Private declarations }
    Fdetail_id: Integer;
    Fmaster_id: Integer;
    Flookup_id: Integer;
    Flookup_description: String;
    Fprice: Double;
    Flookup: Tlookup;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;

    [Restrictions([NoUpdate, NotNull])]
    [Column('detail_id', ftInteger)]
    [Dictionary('ID Detalhe','Mensagem de valida��o','','','',taCenter)]
    property detail_id: Integer Index 0 read Fdetail_id write Fdetail_id;

    [Restrictions([NotNull])]
    [Column('master_id', ftInteger)]
    [Dictionary('ID Mestre','Mensagem de valida��o','','','',taCenter)]
    property master_id: Integer Index 1 read Fmaster_id write Fmaster_id;

    [Restrictions([NotNull])]
    [Column('lookup_id', ftInteger)]
    [ForeignKey('Lookup', None, None)]
    [Dictionary('ID Lookup','Mensagem de valida��o','0','','',taCenter)]
    property lookup_id: Integer Index 2 read Flookup_id write Flookup_id;

    [Column('lookup_description', ftString, 30)]
    [Dictionary('Descri��o Lookup','Mensagem de valida��o','','','',taLeftJustify)]
    property lookup_description: String Index 3 read Flookup_description write Flookup_description;

    [Restrictions([NotNull])]
    [Column('price', ftFloat, 18, 3)]
    [Dictionary('Pre�o Unit�rio','Mensagem de valida��o','0','#,###,##0.00','',taRightJustify)]
    property price: Double Index 4 read Fprice write Fprice;

    property lookup: Tlookup read Flookup write Flookup;
  end;

implementation

{ Tdetail }

constructor Tdetail.Create;
begin
   Flookup := Tlookup.Create;
end;

destructor Tdetail.Destroy;
begin
  Flookup.Free;
  inherited;
end;

initialization
  TRegisterClass.RegisterEntity(Tdetail);

end.
