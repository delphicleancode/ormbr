unit ormbr.model.client;

interface

uses
  Classes, 
  DB, 
  SysUtils, 
  Generics.Collections, 
  /// orm 
  ormbr.mapping.attributes, 
  ormbr.mapping.classes,
  ormbr.types.nullable,
  ormbr.types.lazy,
  ormbr.types.mapping,
  ormbr.mapping.register;

type
  [Entity]
  [Table('client','')]
  [PrimaryKey('client_id', 'Chave prim�ria')]
  [Indexe('idx_client_name','client_name')]
  Tclient = class
  private
    { Private declarations }
    Fclient_id: Integer;
    Fclient_name: String;
    Fclient_name_: String;
  public
    { Public declarations }
    [Restrictions([NoUpdate, NotNull])]
    [Column('client_id', ftInteger)]
    [Dictionary('client_id','Mensagem de valida��o','','','',taCenter)]
    property client_id: Integer Index 0 read Fclient_id write Fclient_id;

    [Column('client_name', ftString, 40)]
    [Dictionary('client_name','Mensagem de valida��o','','','',taLeftJustify)]
    property client_name: String Index 1 read Fclient_name write Fclient_name;

    [Column('client_name_', ftString, 40)]
    [Dictionary('client_name_','Mensagem de valida��o','','','',taLeftJustify)]
    property client_name_: String Index 1 read Fclient_name_ write Fclient_name_;
  end;

implementation

initialization
  TRegisterClass.RegisterEntity(Tclient);

end.
