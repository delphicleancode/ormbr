unit uMainFormORM;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  DB,
  Grids,
  DBGrids,
  StdCtrls,
  Mask,
  DBClient,
  DBCtrls,
  ExtCtrls,
  /// orm factory
  ormbr.factory.interfaces,
  /// orm injection dependency
  ormbr.dependency.interfaces,
  ormbr.dependency.injection.fdmemtable,
  ormbr.factory.firedac,
  ormbr.types.database,
  /// orm model
  ormbr.model.master,
  ormbr.model.detail,
  ormbr.model.lookup,
  ormbr.model.client,

  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Stan.Intf,
  FireDAC.Phys.SQLite, FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs,
  FireDAC.Comp.UI, FireDAC.DApt, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet;

type
  TForm3 = class(TForm)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    DBGrid2: TDBGrid;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    FDConnection1: TFDConnection;
    FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDMaster: TFDMemTable;
    FDDetail: TFDMemTable;
    FDClient: TFDMemTable;
    FDLookup: TFDMemTable;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
    oConn: IDBConnection;
    oMaster: IContainerDataSet<Tmaster>;
    oDetail: IContainerDataSet<Tdetail>;
    oClient: IContainerDataSet<Tclient>;
    oLookup: IContainerDataSet<Tlookup>;
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

uses
  StrUtils;

{$R *.dfm}

procedure TForm3.Button2Click(Sender: TObject);
begin
  oMaster.DataSet.ApplyUpdates(0);
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  oMaster.DataSet.Open;
end;

procedure TForm3.Button4Click(Sender: TObject);
begin
  oMaster.DataSet.Close;
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
  /// <summary>
  /// Variaveis declaradas em { Private declarations } acima.
  /// </summary>

  // Inst�ncia da class de conex�o via FireDAC
  oConn := TFactoryFireDAC.Create(FDConnection1, dnSQLite);

  /// Class Adapter
  /// Par�metros: (IDBConnection, TClientDataSet)
  /// 10 representa a quantidadede registros por pacote de retorno para um select muito grande,
  /// defina o quanto achar melhor para sua necessiade
  oMaster := TContainerFDMemTable<Tmaster>.Create(oConn, FDMaster, 10);

  /// Relacionamento Master-Detail
  oDetail := TContainerFDMemTable<Tdetail>.Create(oConn, FDDetail, oMaster.DataSet);

  /// Adiciona um campo Aggregate
  oDetail.DataSet.AddAggregateField('AGGPRICE','SUM(PRICE)', taRightJustify, '#,###,##0.00');

  /// Relacionamento 1:1
  oClient := TContainerFDMemTable<Tclient>.Create(oConn, FDClient, oMaster.DataSet);

  /// Lookup lista de registro (DBLookupComboBox)
  oLookup := TContainerFDMemTable<Tlookup>.Create(oConn, FDLookup);

  /// Campo LookupField pode ser usado em um DBLookupComboBox, ou DBGrid
  oDetail.DataSet.AddLookupField('fieldname',
                                 'lookup_id',
                                 oLookup.DataSet,
                                 'lookup_id',
                                 'lookup_description');
  oMaster.DataSet.Open;
  /// Outras formas para fazer um open, se precisar
///  oMaster.DataSet.Open(10);
///  oMaster.DataSet.Open(ICriteria.SQL.Select.All.From('Master').OrderBy('description'));
end;

end.
